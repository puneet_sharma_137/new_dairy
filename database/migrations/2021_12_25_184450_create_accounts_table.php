<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id')->nullable();
            $table->string('image',255)->nullable();
            $table->mediumText('vendor_code')->nullable();
            $table->string('vendor_name',255)->nullable();
            $table->string('father',255)->nullable();
            $table->string('address',255)->nullable();
            $table->string('phone_number',255)->nullable();
            $table->string('mobile_number',255)->nullable();
            $table->string('city',255)->nullable();
            $table->string('gst_number',255)->nullable();
            $table->string('email_address',255)->nullable();
            $table->string('pan_number',255)->nullable();
            $table->string('aadhaar_number',255)->nullable();
            $table->string('bank_name',255)->nullable();
            $table->string('bank_branch',255)->nullable();
            $table->string('account_number',255)->nullable();
            $table->string('ifsc_number',255)->nullable();
            $table->string('rate',255)->nullable();
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
