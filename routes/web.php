<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\accountController;
use App\Http\Controllers\ProfileController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return view('layouts/main');
});

Route::view('register','register');
Route::view('addaccount','addaccount');
Route::view('forgot_password', 'auth.reset_password')->name('password.reset');



Route::get('pdf{user_id}',[accountcontroller::class,'list']);
// Route::view('pdf','pdf');
Route::get('/bill/pdf_pdf{user_id}',[accountController::class,'pdf']);
Route::get('/bill/pdf_account_list{user_id}',[accountController::class,'pdf_account_list']);// Pankaj 
Route::get('pdf_pdf{user_id}',[accountController::class,'pdf_mob']);


Route::any('upload_image{user_id}',[ProfileController::class,'addprofile2']);

// 15/11/2021 // 
// Route::post('new_tab_upload_image{user_id}',[TaskController::class,'new_tab_upload_image']);
Route::any('/new_tab_upload_image','TaskController@new_tab_upload_image')->name('new_tab_upload_image');

Route::any('/add_account_upload_image','accountController@add_account_upload_image')->name('add_account_upload_image');


Route::get('export', 'accountController@export')->name('export');
Route::get('importExportView', 'accountController@importExportView');
Route::post('import', 'accountController@import')->name('import');

Route::get('excelData{user_id}',[accountController::class,'excelData']);


// EXCEL DOWNLOADS
Route::get('excelDownload{id}', 'accountController@excelDownload')->name('excelDownload');
Route::get('bankAccountCSV{id}', 'accountController@bankAccountCSV');

Route::get('milkCollection{id}', 'ExcelController@milkcollection');
Route::get('milk_sale{id}', 'ExcelController@milk_sale');
Route::get('payments{id}', 'ExcelController@payments');
Route::get('stock_update{id}', 'ExcelController@stock_update');
Route::get('dispatchExcel{id}', 'ExcelController@dispatchExcel');
Route::get('deductExcel{id}', 'ExcelController@deductExcel');
Route::get('bonusExcel{id}', 'ExcelController@bonusExcel');
Route::get('itemSalesExcel{id}', 'ExcelController@itemSalesExcel');


Route::get('PurchaseReportExcel{id}', 'ExcelController@PurchaseReportExcel');
Route::get('SaleReportExcel{id}', 'ExcelController@SaleReportExcel');
Route::get('StockReportExcel{id}', 'ExcelController@StockReportExcel');

Route::get('AccReport/{id}', 'DatatableController@accountexport');
Route::get('bank/{id}', 'DatatableController@bank');
Route::get('MilkSales/{id}', 'DatatableController@milksalesexport');
Route::get('itemsales/{id}', 'DatatableController@itemsalesexport');
Route::get('stockupdates/{id}', 'DatatableController@stockupdatesexport');
Route::get('dispatches/{id}', 'DatatableController@dispatchesexport');
Route::get('payment/{id}', 'DatatableController@paymentexport');
Route::get('deduct/{id}', 'DatatableController@deductexport');
Route::get('bonus/{id}', 'DatatableController@bonusexport');
Route::get('stockexport/{id}', 'DatatableController@stockexport');
// Route::get('Payment{id}', 'ExcelController@bonusExcel');
Route::get('milkcollectionexport/{id}', 'DatatableController@milkcollectionexport');

//
Route::any('upload_excel_file{id}', 'TaskController@upload_excel_file')->name('upload_excel_file');

Route::get('excel_list{id}', 'TaskController@excel_list');

Route::get('CowUploadview{id}','DataUploadController@CowUploadview')->name('CowUploadview');
Route::get('BuffUploadview{id}','DataUploadController@BuffUploadview')->name('BuffUploadview');

Route::get('helpFaq','AdminUserController@helpFaq')->name('helpFaq');
Route::get('helpServices','AdminUserController@helpServices')->name('helpServices');
Route::get('helpHelpline','AdminUserController@helpHelpline')->name('helpHelpline');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
// Route::view('/user', 'HomeController@user')->name('user');

Auth::routes();

Route::get('/home', function() {
    return view('home');
})->name('home')->middleware('auth');
Route::get('/user', 'AdminUserController@index')->name('users')->middleware('auth');
Route::any('user/{id}/edit','AdminUserController@editUser')->name('user.edit')->middleware('auth');
Route::any('user/{id}/edit_status','AdminUserController@editUserStatus')->name('user.edit_status')->middleware('auth');
Route::post('user/{id}/update','AdminUserController@updateUser')->name('user.update')->middleware('auth');
Route::any('user/{id}/update_status','AdminUserController@updateStatus')->name('user.update_status')->middleware('auth');
Route::get('/users/list', 'AdminUserController@getUsers')->name('get.users')->middleware('auth');

Route::get('getUsersDuePayment', 'AdminUserController@getUsersDuePayment')->name('get.getUsersDuePayment')->middleware('auth');
Route::get('logoutall', 'AdminUserController@logoutall')->name('get.logoutall');


Route::get('/PaymentTransaction', 'AdminUserController@PaymentTransaction')->name('get.PaymentTransaction')->middleware('auth');
Route::get('/milksale', 'AdminUserController@milksale')->name('get.milksale')->middleware('auth');
Route::get('/homebanner', 'AdminUserController@homebanner')->name('homebanner')->middleware('auth');
Route::post('/uploadimages', 'AdminUserController@uploadimages')->name('uploadimages')->middleware('auth');
Route::get('/homeimageslisting', 'AdminUserController@homeimageslisting')->name('homeimageslisting')->middleware('auth');
Route::DELETE ('/destroyimages/{id}', 'AdminUserController@destroyimages')->name('destroyimages')->middleware('auth');


Route::get('/add_starpoints', 'AdminUserController@add_starpoints')->name('add_starpoints')->middleware('auth');
Route::post('/save_starpoints', 'AdminUserController@save_starpoints')->name('save_starpoints')->middleware('auth');
Route::get('/starpoint', 'AdminUserController@starpoint')->name('starpoint')->middleware('auth');
Route::DELETE ('/destroypoints/{id}', 'AdminUserController@destroypoints')->name('destroypoints')->middleware('auth');




Route::get('/payment_due', function() {
    return view('payment_due');
})->name('get.payment_due')->middleware('auth');

Route::get('/account', function() {
    return view('account');
})->name('get.account')->middleware('auth');
