<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\accountController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\PasswordResetRequestController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\rateController;
use App\Http\Controllers\reportscontroller;
use App\Http\Controllers\settingController;
use App\Http\Controllers\SendEmailController;
use App\Http\Controllers\TabController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//set up
// ***************LOGIN/REGISTER******************************
Route::post('register',[LoginController::class,'register']);
Route::post('login',[LoginController::class,'login']);
Route::post('paymentstatus',[LoginController::class,'paymentstatus']);
Route::post('changepassword',[LoginController::class,'changepassword']);

Route::post('addTabToFavourite',[TabController::class,'addTabToFavourite']);
Route::post('getFavouriteTabs',[TabController::class,'getFavouriteTabs']);
Route::post('getFavouriteByTabName',[TabController::class,'getFavouriteByTabName']);


// *********************addAccount*****************************
Route::post('deleteSelectedAccounts',[accountController::class,'deleteSelectedAccounts']);
Route::post('deleteSelectedBankAccounts',[accountController::class,'deleteSelectedBankAccounts']);
Route::post('addaccount',[accountController::class,'addaccount']);
Route::post('account_list',[accountController::class,'accountlist']);
Route::post('updateAccount',[accountcontroller::class,'updateAccount']);
Route::post('newaccount',[accountController::class,'newaccount']);
Route::post('updateccount',[accountcontroller::class,'updateccount']);
Route::post('accountlistsingle',[accountcontroller::class,'accountlistsingle']);
Route::post('filter1',[accountcontroller::class,'filter1']);
Route::post('downloadpdf',[accountcontroller::class,'downloadPDF']);

Route::post('UploadData',[accountcontroller::class,'UploadData']);

Route::get('accountcsv',[accountcontroller::class,'accountcsv']);


Route::post('password',[ChangePasswordController::class,'password']);




// ********************Profile*********************************
Route::post('addprofile',[ProfileController::class,'addprofile']);
Route::post('profile_list',[ProfileController::class,'profilelist']);
Route::post('updateProfile',[ProfileController::class,'updateProfile']);


// ********************Forgot Password**************************
Route::post('password/email', [ForgotPasswordController::class,'forgot']);
Route::post('password/reset', [ForgotPasswordController::class,'reset']);

// ************************Milk Collection**********************
Route::post('addmilkcollection',[TaskController::class,'addmilkcollection']);
Route::post('milkcollection_list',[TaskController::class,'milkcollectionlist']);
Route::post('updatemilkcollection',[TaskController::class,'updatemilkcollection']);


// ***********************Milk Sales****************************
Route::post('addmilksales',[TaskController::class,'addmilksales']);
Route::post('milksaleslistsingle',[TaskController::class,'milksaleslistsingle']);
Route::post('milksalesfilter',[TaskController::class,'milksalesfilter']);


Route::post('milksales_list',[TaskController::class,'milksaleslist']);
Route::post('updatemilksales',[TaskController::class,'updatemilksales']);
Route::post('milkcollectionlistsingle',[TaskController::class,'milkcollectionlistsingle']);
Route::post('milkunit1',[TaskController::class,'unit1']);
Route::post('milkunit2',[TaskController::class,'unit2']);
Route::post('milkfilter',[TaskController::class,'milkfilter']);





Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// ************************Home Controller ***********************
Route::post('addnote',[HomeController::class,'addnote']);
Route::post('note_list',[HomeController::class,'notelist']);

Route::post('addstarpoints',[HomeController::class,'addstarpoints']);
Route::get('starpoints_list',[HomeController::class,'starpointslist']);

Route::post('addhomeimages',[HomeController::class,'addhomeimages']);
Route::get('homeimages_list',[HomeController::class,'homeimageslist']);
Route::post('deleteMilkcollection',[TaskController::class,'deleteMilkcollection']);
Route::post('deleteMilksales',[TaskController::class,'deleteMilksales']);
Route::post('updateitemsale',[TaskController::class,'updateitemsale']);





Route::post('deleteitemsales',[TaskController::class,'deleteitemsales']);
Route::post('additemsales',[TaskController::class,'additemsales']);
Route::post('itemsalesingle',[TaskController::class,'itemsalesingle']);
Route::post('itemsalelist',[TaskController::class,'itemsalelist']);
Route::post('deleteitemsale',[TaskController::class,'deleteitemsale']);
Route::post('itemsalevendor',[TaskController::class,'itemsalevendor']);

Route::post('itemfilter',[TaskController::class,'itemfilter']);

Route::post('addmilkcollectionnew',[TaskController::class,'addmilkcollectionnew']);










Route::post('adddesktoppayments',[TaskController::class,'adddesktoppayments']);
Route::post('addpayments',[TaskController::class,'addpayments']);
Route::post('desktoppayments_list',[TaskController::class,'desktoppayments_list']);
Route::post('payments_list',[TaskController::class,'paymentslist']);
Route::post('updatepayments',[TaskController::class,'updatepayments']);
Route::post('updatedesktoppayments',[TaskController::class,'updatedesktoppayments']);
Route::post('deletepayments',[TaskController::class,'deletepayments']);
Route::post('deletedesktoppayments',[TaskController::class,'deletedesktoppayments']);
Route::post('desktoppaymentlistsingle',[TaskController::class,'desktoppaymentlistsingle']);
Route::post('stockupdatesingle',[TaskController::class,'stockupdatesingle']);




Route::post('addstockupdates',[TaskController::class,'addstockupdates']);
Route::post('stockupdates_list',[TaskController::class,'stockupdateslist']);
Route::post('updatestockupdates',[TaskController::class,'updatestockupdates']);
Route::post('deletestockupdates',[TaskController::class,'deletestockupdates']);

Route::post('adddispatch',[TaskController::class,'adddispatch']);
Route::post('dispatch_list',[TaskController::class,'dispatchlist']);
Route::post('updatedispatch',[TaskController::class,'updatedispatch']);
Route::post('deletedispatch',[TaskController::class,'deletedispatch']);
Route::post('dispatchupdatesingle',[TaskController::class,'dispatchupdatesingle']);

Route::post('adddeducations',[TaskController::class,'adddeducations']);
Route::post('deducations_list',[TaskController::class,'deducationslist']);
Route::post('updatededucations',[TaskController::class,'updatededucations']);
Route::post('deletededucations',[TaskController::class,'deletededucations']);
Route::post('deducationsingle',[TaskController::class,'deducationsingle']);
Route::post('deducationfilter',[TaskController::class,'deducationfilter']);

Route::post('addbonus',[TaskController::class,'addbonus']);
Route::post('bonuslist',[TaskController::class,'bonuslist']);
Route::post('bonussingle',[TaskController::class,'bonussingle']);
Route::post('updatebonus',[TaskController::class,'updatebonus']);
Route::post('deletebonus',[TaskController::class,'deletebonus']);








Route::post('addrate',[rateController::class,'addrate']);
Route::post('ratelist',[rateController::class,'ratelist']);
Route::post('updaterate',[rateController::class,'updaterate']);
Route::post('searchbydate',[TaskController::class,'searchbydate']);
Route::post('paymentfilter',[TaskController::class,'paymentfilter']);
Route::post('stockupdatefilter',[TaskController::class,'stockupdatefilter']);
Route::post('dispatchfilter',[TaskController::class,'dispatchfilter']);



Route::post('dislist',[TaskController::class,'dislist']);
Route::post('dislist2',[TaskController::class,'dislist2']);
Route::post('dislist3',[TaskController::class,'dislist3']);

Route::post('itemsalelist',[TaskController::class,'itemsalelist']);








Route::post('fat_rate',[rateController::class,'fat_rate']);
Route::post('fat_snf_rate',[rateController::class,'fat_snf_rate']);
Route::post('fat_rate_list',[rateController::class,'fat_rate_list']);

Route::post('delete_advance_rate',[rateController::class,'delete_advance_rate']);






Route::post('weight',[settingController::class,'weight']);
Route::post('getweight',[settingController::class,'getweight']);
Route::post('updateweight',[settingController::class,'updateweight']);

Route::post('getanalyzer',[settingController::class,'getanalyzer']);
Route::post('getanalyzername',[settingController::class,'getanalyzername']);

Route::post('addanalyzerdata',[settingController::class,'addanalyzerdata']);

Route::post('updateanalyzerdata',[settingController::class,'updateanalyzerdata']);

Route::post('addsetting',[settingController::class,'addsetting']);

Route::post('getsetting',[settingController::class,'getsetting']);

Route::post('updatesetting',[settingController::class,'updatesetting']);


Route::post('addanalyzersetting',[settingController::class,'addanalyzersetting']);

Route::post('getanalyzersetting',[settingController::class,'getanalyzersetting']);

Route::post('updateanalyzersetting',[settingController::class,'updateanalyzersetting']);







// help********************************************************

Route::post('help_faq',[TaskController::class,'help_faq']);
Route::post('help_faq_list',[TaskController::class,'help_faq_list']);
Route::post('help_services',[TaskController::class,'help_services']);
Route::post('help_service_list',[TaskController::class,'help_service_list']);
Route::post('help_helpline',[TaskController::class,'help_helpline']);
Route::post('help_helpline_list',[TaskController::class,'help_helpline_list']);

Route::get('send_note',[SendEmailController::class,'send']);




// 17/11/2021

Route::post('CowUploadFile{id}','DataUploadController@CowUploadFile')->name('CowUploadFile');
Route::post('BuffUploadFile{id}','DataUploadController@BuffUploadFile')->name('BuffUploadFile');

Route::post('get_uploded_file','DataUploadController@get_uploded_file')->name('get_uploded_file');

Route::post('BuffDeleteFile','DataUploadController@BuffDeleteFile')->name('BuffDeleteFile');

Route::post('GetDataValue','DataUploadController@GetDataValue')->name('GetDataValue');

Route::post('FileAssign','DataUploadController@FileAssign')->name('FileAssign');

Route::post('AssignedFiles','DataUploadController@AssignedFiles')->name('AssignedFiles');

Route::post('FileList','DataUploadController@FileList')->name('FileList');
Route::post('NewFlatRate','DataUploadController@NewFlatRate')->name('NewFlatRate');
Route::post('LatestRate','DataUploadController@LatestRate')->name('LatestRate');
Route::post('CheckFlatRate','DataUploadController@CheckFlatRate')->name('CheckFlatRate');

Route::post('CheckFileRate','DataUploadController@CheckFileRate')->name('CheckFileRate');

// Route::post('CowUploadview','DataUploadController@CowUploadview')->name('CowUploadview');


Route::post('PaymentUpdate','PaymentController@PaymentUpdate')->name('PaymentUpdate');

Route::post('AnalyzerData','AnalyzerDataController@AnalyzerData')->name('AnalyzerData');
Route::post('AnalyzerDataList','AnalyzerDataController@AnalyzerDataList')->name('AnalyzerDataList');
Route::post('WeighingData','AnalyzerDataController@WeighingData')->name('WeighingData');
Route::post('WeighingDataList','AnalyzerDataController@WeighingDataList')->name('WeighingDataList');






// *********************************************Report******************************************

Route::post('milkcollectionlistreport',[reportscontroller::class,'milkcollectionlistreport']);
Route::post('updatemilkcollectionreport',[reportscontroller::class,'updatemilkcollectionreport']);
Route::post('deleteMilkcollectionreport',[reportscontroller::class,'deleteMilkcollectionreport']);

Route::post('milksaleslistreport',[reportscontroller::class,'milksaleslistreport']);
Route::post('updatesalesreport',[reportscontroller::class,'updatemilkcollectionreport']);
Route::post('deleteMilkcollectionreport',[reportscontroller::class,'deleteMilkcollectionreport']);

Route::post('milksaleslistreport',[reportscontroller::class,'milksaleslistreport']);
Route::post('updatesalesreport',[reportscontroller::class,'updatemilkcollectionreport']);
Route::post('deleteMilkcollectionreport',[reportscontroller::class,'deleteMilkcollectionreport']);



Route::post('reportstockupdateslist',[reportscontroller::class,'reportstockupdateslist']);
Route::post('reportdesktoppayments_list',[reportscontroller::class,'reportdesktoppayments_list']);
Route::post('reportitemsalelist',[reportscontroller::class,'reportitemsalelist']);
Route::post('reportdispatchlist',[reportscontroller::class,'reportdispatchlist']);




Route::post('milkvendorfilter',[reportscontroller::class,'milkvendorfilter']);
Route::post('reportfilter1',[reportscontroller::class,'reportfilter1']);

Route::post('reportfiltermilksale',[reportscontroller::class,'reportfiltermilksale']);
// Route::post('reportfilter1',[reportscontroller::class,'reportfilter1']);
// Route::post('reportfiltermilksale',[reportscontroller::class,'reportfiltermilksale']);
Route::post('reportfilteritemsale',[reportscontroller::class,'reportfilteritemsale']);
Route::post('reportfilterstockupdate',[reportscontroller::class,'reportfilterstockupdate']);
Route::post('reportfilterdispatch',[reportscontroller::class,'reportfilterdispatch']);

Route::post('reportfilterpayment',[reportscontroller::class,'reportfilterpayment']);

Route::post('bill/pdf_pdf{user_id}',[accountController::class,'pdf_mob']);

Route::post('account_pdf_mob',[accountController::class,'account_pdf_mob']);

Route::post('accountfilter',[accountController::class,'accountfilter']);

Route::post('accountapi',[accountController::class,'accountAPI']);


// Store XL & Fetch XL
Route::post('store_xl',[TaskController::class,'store_xl']);
Route::post('get_xl',[TaskController::class,'get_xl']);


// Store Profile & Fetch Profile
Route::post('store_profile',[ProfileController::class,'store_profile']);
Route::post('get_profile',[ProfileController::class,'get_profile']);


// *********************************************End Reports*************************************

// ***********************************************************************************************
 
   
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
   
    Route::post('sendPasswordResetLink', [PasswordResetRequestController::class,'sendEmail']);
    Route::post('resetPassword', [ChangePasswordController::class,'passwordResetProcess']);
    
});

Route::post('sendEmail', [MailController::class,'sendEmail']);


// ***********************************************************************************************