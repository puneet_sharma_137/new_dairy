<?php
require_once("PaytmKit/lib/config_paytm.php");
require_once("PaytmKit/lib/encdec_paytm.php");

    // define("merchantMid", "dZlzzF17647713571019");
    // Key in your staging and production MID available in your dashboard
    // define("merchantKey", "O0zUdI1&G%OQViK_");
    // Key in your staging and production merchant key available in your dashboard
    // define("orderId", "order1");
    // define("channelId", "WEB");
    // define("website", "WEBSTAGING");
    // define("industryTypeId", "Retail");
    // define("callbackUrl", "http://127.0.0.1/devchandan/payment-using-paytm/response.php");
    // define("txnAmount", "100.12");
    // This is the staging value. Production value is available in your dashboard
    // This is the staging value. Production value is available in your dashboard
    // define("callbackUrl", "https://<Merchant_Response_URL>");

if(isset($_GET['txnAmount']))
{
   $txnAmount =  $_GET['txnAmount'];
}
else
{
    $txnAmount 	= "10.50"; 
}

if(isset($_GET['email']))
{
   $email =  $_GET['email'];
}
else
{
    $email 	= "username@emailprovider.com";
}


    if(isset($_GET['custId']))
    {
       $custId =  $_GET['custId'];
    }
    else
    {
        $custId 	= "123";
    }


    if(isset($_GET['mobileNo']))
    {
       $mobileNo =  $_GET['mobileNo'];
    }
    else
    {
        $mobileNo 	= "7777777777";
    }



    $orderId 	= $custId.'_'.time();
   
   
   
   

    $paytmParams = array();
    $paytmParams["ORDER_ID"] 	= $orderId;
    $paytmParams["CUST_ID"] 	= $custId;
    $paytmParams["MOBILE_NO"] 	= $mobileNo;
    $paytmParams["EMAIL"] 		= $email;
    $paytmParams["TXN_AMOUNT"] 	= $txnAmount;
    $paytmParams["MID"] 		= PAYTM_MERCHANT_MID;
    $paytmParams["CHANNEL_ID"] 	= PAYTM_CHANNEL_ID;
    $paytmParams["WEBSITE"] 	= PAYTM_MERCHANT_WEBSITE;
    $paytmParams["INDUSTRY_TYPE_ID"] = PAYTM_INDUSTRY_TYPE_ID;
    $paytmParams["CALLBACK_URL"] = PAYTM_CALLBACK_URL;
    $paytmChecksum = getChecksumFromArray($paytmParams, PAYTM_MERCHANT_KEY);
    $transactionURL = PAYTM_TXN_URL;
    // $transactionURL = "https://securegw-stage.paytm.in/theia/processTransaction";
    // $transactionURL = "https://securegw.paytm.in/theia/processTransaction"; // for production
?>
<html>
    <head>
        <title>Merchant Checkout Page</title>
        
        <style>
            .center_pay_btn{
                width:100%;
                float:left;
                text-align: center;
            }
            
            .center_pay_btn input{
                background-color: #00b9f5;
                color: #fff;
                border: 1px solid #00b9f5;
                padding: 10px 35px;
                font-size: 18px;
            }
            
            .top_padd{
                margin-top: 80px;
            } 
            
        </style>
    </head>
    <body>
        <center class="top_padd"><h1>Please do not refresh this page...</h1></center>
        <form method='post' action='<?php echo $transactionURL; ?>' name='f1'>
            <?php
                foreach($paytmParams as $name => $value) {
                    echo '<input type="hidden" name="' . $name .'" value="' . $value . '">';
                }
            ?>
            <input type="hidden" name="CHECKSUMHASH" value="<?php echo $paytmChecksum ?>">
            <div class="center_pay_btn">
                <input type="submit" name="submit" value="Pay" />
            </div>
            
        </form>
        <!-- <script type="text/javascript">
            document.f1.submit();
        </script> -->
    </body>
</html>




