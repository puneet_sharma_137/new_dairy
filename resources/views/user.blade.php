@extends('adminlte::page')
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
@show

@section('content')
<div class="container">
    <h1>User List</h1>
    <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>Sr No</th>
                <th>Name</th>
                <th>Lname</th>
                <th>email </th>
                <th>phone_number</th>
                <th>password</th>
                <th>status</th> 
                <th width="100px">Action</th>
            </tr>
        </thead>
        <tbody>
    
        </tbody>
    </table>
    
    <!-- boostrap update user model -->
<div class="modal fade" id="userModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form id="ItemForm" name="ItemForm" class="form-horizontal">
                    @csrf
                    <input type="hidden" name="Item_id" class="Item_id">
                    <div class="form-group">
                        <label for="name" class="col-sm-12 control-label">User Name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter User Name"
                                maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-12 control-label">User Last Name</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="lname" name="lname"
                                placeholder="Enter Last Name" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-12 control-label"> Email</label>
                        <div class="col-sm-12">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email"
                                maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 control-label">Phone Number</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="phone_number" name="phone_number"
                                placeholder="Phone Number" required="">
                        </div>
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end bootstrap model -->
</div>

    <!-- boostrap Active/inactive user model -->
<div class="modal fade" id="statusModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="statusHeading"></h4>
            </div>
            <div class="modal-body">
                <h6 class ="message">Are you sure ? Do you want to change the status ? </h6>
                
                <form id="statusForm" name="statusForm" class="form-horizontal">
                    @csrf
                    <input type="hidden" name="Item_id" class="Item_id">
                    <div class="form-group">
                        <label for="name" class="col-sm-12 control-label">Status</label>
                        <div class="col-sm-12">
                          <select name="is_active" class="is_active">
                              <option value="1">Active</option> 
                                <option value="0">Inactive</option> 
                        </select>          
                        </div>
                    </div>
 
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary " id ="update_status">Yes
                        </button>
                         <button type="button" class="btn btn-primary"  value="create">No
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end bootstrap model -->
</div>
   
@endsection

@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
  $(function () {
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('get.users') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'lname', name: 'lname'},
            {data: 'email', name: 'email'},
            {data: 'phone_number', name: 'phone_number'},
            {data: 'password', name: 'password'},
            {data: 'isActive', name: 'isActive'},
           
            {
                data: 'action', 
                name: 'action', 
                orderable: true, 
                searchable: true
            },
        ]
    });
    
  });

  $('body').on('click', '.editUser', function () {
      var user_id = $(this).data('id');
      $.get("{{ url('user') }}" +'/' + user_id +'/edit', function (data) {
        $('#modelHeading').html("Edit User");
        $('#user-modal').modal('show');
        $('.Item_id').val(data.id);
        $('#name').val(data.name);
        $('#lname').val(data.lname);
        $('#phone_number').val(data.phone_number);
        $('#email').val(data.email);
      })
   });
   

     $('#saveBtn').click(function (e) {
        e.preventDefault();
        $(this).html('Sending..');
   var item_id =  $('.Item_id').val();
        $.ajax({
          data: $('#ItemForm').serialize(),
          url: "user/" + item_id + '/update',
          type: "POST",
          dataType: 'json',
          success: function (data) {
     
              $('#ItemForm').trigger("reset");
              $('#userModel').modal('hide');
            //   table.draw();
         
          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
      
    
     });

  $('body').on('click', '.editStatus', function () {
      var user_id = $(this).data('id');
    
      $.get("{{ url('user') }}" +'/' + user_id +'/edit', function (data) {
        $('#statusHeading').html("Edit User");
        $('#user-modal').modal('show');
        $('.Item_id').val(data.id);
     
      })
   });
     
     $('#update_status').click(function(e){
          e.preventDefault();
         var status=  $('#statusForm').serialize()
        //   console.log(status);
              var item_id =  $('.Item_id').val();
        //      var is_active =  $('.is_active').val();
        //   console.log(is_active);
        // console.log('v');return; 
          $.ajax({
          data: $('#statusForm').serialize(),
          url: "user/" + item_id + '/update_status',
          type: "POST",
          dataType: 'json',
          success: function (data) {
     console.log(data);
              $('#statusForm').trigger("reset");
              $('#statusModel').modal('hide');
           location.reload();
         
          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
      });
      
     });
</script>
@stop

