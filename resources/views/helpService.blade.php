@extends('adminlte::page')
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
@show

@section('content')
<div class="container">
    <h1>Services Listing</h1>
    <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>User Name</th>
                <th>Note</th>
                <th>Date Time</th>
            </tr>
        </thead>
        <tbody>
            @php $totalrecord = count($servicelist);
            
            if($totalrecord==0)
            { echo "No Record Found"; } @endphp
           @if(!empty($servicelist))  

                        @php          
                        $RowNumber=0;
                        @endphp  

                        @foreach ($servicelist as $helplist) 

				    <tr>
                <td>{{ $helplist->id}}</td>
                <td>{{ $helplist->name}} {{ $helplist->lname}}</td>
                <td>{{ $helplist->note}}</td>
                <td>{{ $helplist->created_at}} </td>
               
            </tr>
             @endforeach
              @endif
        </tbody>
    </table>

    
  
</div>
<div style="padding-top:40px;">
   {{ $servicelist->links()}}
</div>
   </div>
@endsection

@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
  $(function () {
    
    var table = $('.data-table').DataTable({
       "paging":   false,
        "ordering": false,
        "info":     false,
         "bFilter": false,
        "bInfo": false
    });
    
  });

  
</script>
@stop

