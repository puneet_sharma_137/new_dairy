@extends('adminlte::page')
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
@show

@section('content')



<div class="card">
    <div class="card-header">
    Home Images
    </div>
	 @if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
	
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif
    <div class="card-body">
        <form action="{{ route("uploadimages")  }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">Upload images</label>
               	<input  type="file" id="bannerimages" name="bannerimages[]" multiple />
                @if($errors->has('bannerimages'))
                    <em class="invalid-feedback">
                        {{ $errors->first('bannerimages') }}
                    </em>
                @endif
                <p class="helper-block">
                  
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="Upload">
            </div>
        </form>
    </div>
</div>
   
@endsection

@section('js')

@stop

