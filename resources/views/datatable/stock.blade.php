<!DOCTYPE html>
<html lang="en">


<head>
        <meta charset="utf-8">
          <title>Dairy </title>
        <link rel="stylesheet" href="style.css" type="text/css">
        <link rel="icon" href="images/favicon.png" type="image/png" sizes="24x24">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <!-- Bootstrap 4 CDN link -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
        <!-- Bootstrap 4 Files -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>

<body>
  <!-- Start Page Loading -->


  <div class="container-fluid login_page">
    <div class="row">
        <div class="col-md-12">
          <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
        </div>
      <table id="tblActivityMaster" class="table table-bordered">
            <thead class="thead-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">CODE</th>
                <th scope="col">NAME</th>
                <th scope="col">NOMINEE</th>
                <th scope="col">CITY</th>
                <th scope="col">ADDRESS</th>
                <th scope="col">PHONE NO.</th>
                <th scope="col">MOBILE</th>
                <th scope="col">UPI/GST</th>
                <th scope="col">EMAIL</th>
                <th scope="col">PAN</th>
                <th scope="col">AADHAR</th>
                <th scope="col">BANK BRANCH</th>
                <th scope="col">ACCOUNT</th>
                <th scope="col">IFSC</th>
                
              </tr>
            </thead>
            <tbody>
              @foreach($account as $key=>$value)
              <tr>
                <th scope="row">{{$key+1}}</th>
                <td>{{$value->vendor_code}}</td>
                <td>{{$value->vendor_name}}</td>
                <td>{{$value->father}}</td>
                <td>{{$value->city}}</td>
                <td>{{$value->address}}</td>
                <td>{{$value->phone_number}}</td>
                <td>{{$value->mobile_number}}</td>
                <td>{{$value->gst_number}}</td>
                <td>{{$value->email_address}}</td>
                <td>{{$value->pan_number}}</td>
                <td>{{$value->aadhaar_number}}</td>
                <td>{{$value->bank_branch}}</td>
                <td>{{$value->account_number}}</td>
                <td>{{$value->ifsc_number}}</td>
              
              </tr>
              @endforeach
             
            </tbody>
          </table>
        </div>
    </div>
  </div>


</body>
<script src="{{asset('/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('/datatable/jquery.dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('/datatable/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('/datatable/jszip.min.js')}}"></script>
<script src="{{asset('/datatable/buttons.flash.min.js')}}"></script>
<script src="{{asset('/datatable/buttons.html5.min.js')}}"></script>
<script src="{{asset('/datatable/buttons.print.min.js')}}"></script>
<script src="{{asset('/datatable/buttons.colVis.min.js')}}"></script>
<script src="{{asset('/datatable/pdfmake.min.js')}}"></script>
<script src="{{asset('/datatable/vfs_fonts.js')}}"></script>
<script src="{{asset('/datatable/dataTables.select.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function() {
			var myTable = 
			$('#tblActivityMaster')
				.DataTable( {
					bAutoWidth: true,
					"aaSorting": [],
					"iDisplayLength": 50
				} );

				$.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

				new $.fn.dataTable.Buttons( myTable, {
					buttons: [
					{
						"extend": "copy",
						"text": "<i class='fa fa-copy bigger-110 pink' title='Copy to clipboard'></i>",
						"className": "btn btn-white btn-primary btn-bold"
					},
					{
						"extend": "csv",
						"text": "<i class='fa fa-database bigger-110 orange' title='Export to CSV'></i>",
						"className": "btn btn-white btn-primary btn-bold",
						title: 'Report'
					},
					{
						"extend": "excel",
						"text": "<i class='fa fa-file-excel-o bigger-110 green' title='Export to Excel'></i>",
						"className": "btn btn-white btn-primary btn-bold",
						title: 'Report'
					},
					{
						"extend": "pdf",
						"text": "<i class='fa fa-file-pdf-o bigger-110 red' title='Export to PDF'></i>",
						"className": "btn btn-white btn-primary btn-bold",
						orientation: 'landscape',
	                	pageSize: 'LEGAL',
						title: 'Report'
					},
					{
						"extend": "print",
						"text": "<i class='fa fa-print bigger-110 grey' title='Print'></i>",
						"className": "btn btn-white btn-primary btn-bold",
						autoPrint: false
					}       
					]
				} );
				myTable.buttons().container().appendTo( $('.tableTools-container') );
  });
</script>
</html>