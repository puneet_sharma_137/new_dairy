@extends('adminlte::page')
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
@show

@section('content')



<div class="card">
    <div class="card-header">
    Star Point
    </div>
	 @if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
	
    @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
    @endif
    <div class="card-body">
        <form action="{{ route("save_starpoints")  }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('points') ? 'has-error' : '' }}">
                <label for="title"> Title </label>
               	<input  type="text" style="width:800px;" id="points" name="points"  value="{{old('points')}}" />
                @if($errors->has('points'))
                    <em class="invalid-feedback">
                        {{ $errors->first('points') }}
                    </em>
                @endif
                <p class="helper-block">
                  
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
   
@endsection

@section('js')

@stop

