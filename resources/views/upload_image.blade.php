<!doctype html>

<html>

<head>
<style>
.myDiv {
  border: 5px outset red;
  background-color: lightblue;   
  padding :50px;
  margin : 10px;
  text-align: center;
}
</style>

</head>

<body>

<form action="{{request()->path()}}" method="post" enctype="multipart/form-data">
@csrf

<div class="myDiv">

<div class="form-group">
        <input type="file" name="image" required>
    </div>
<br>
<br>
<br>
<button type="submit" class="btn btn-primary mr-2">Add Image </button>
</form>

</div>
@if($uploadedImage)
<div>
    <img src="{{url('/') .'/public/images/user/'.$uploadedImage}}"/>
</div>
@endif



</body>




</html>