<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
      </head>
      
        
      <form method="POST" action="{{'/api/BuffUploadFile'. $id}}" enctype="multipart/form-data">
          @csrf
          Select EXCEL to upload:
          <input type="hidden" name="user_id" value="{{$id}}">
          
           <select name="file_type" id="file_type">
  <option value="Buff">Buff</option>
  <option value="Cow">Cow</option>
  
</select>
           <input type="hidden" name="file_id" value="{{ rand(10, 999999)}}">
          <input type="file" name="fileToUpload">
          <input type="submit" name="submit">
      </form>
    </body>
</html>