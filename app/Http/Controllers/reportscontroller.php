<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\milkcollection;
use App\milksale;
use App\stockupdate;
use App\desktoppayment;
use App\itemsale;
use App\dispatch;
use App\account;
use Carbon\Carbon;
use DB;

class reportscontroller extends Controller
{
    //


    function milkcollectionlistreport(Request $req)
    {
        $user_id = $req->post('user_id');
        



        $milkcollection_list = milkcollection::where(['user_id' => $user_id])->paginate(20);
        if (isset($milkcollection_list['0']->id)) {
            return response()->json([
                'status' => 200,
                'paginate_data'   => $milkcollection_list,
                'message' => 'Milk Collection listed sucessfully.',

            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }


    public function updatemilkcollectionreport(Request $req)
    {
        $data = $req->input();

        if ($req->hasFile('image')) {

            $destination_path = 'public/images/user';
            $image = $req->file('image');
            $image_name = $image->getClientOriginalName();
            $path = $req->file('image')->storeAs($destination_path, $image_name);

            $data['image'] = $image_name;
        }

        $milkcollection = milkcollection::where('id', $data['id'])->update($data);

        if ($milkcollection) {

            return response()->json([

                'status' => 200,
                'data'   => $data,
                'message' => 'Milk Collection  updated sucessfully.',



            ]);
        } else {

            return response()->json([

                'status' => 400,

                'message' => 'Error in updating.',


            ]);
        }
    }


    public function deleteMilkcollectionreport(Request $request)

    {


        $id = $request->id;
        DB::table("milkcollections")->whereIn('id', explode(",", $id))->delete();
        return response()->json([

            'status' => 200,

            'message' => 'Milk Collection  Deleted sucessfully.',



        ]);
    }




    //****************************************************Milk Sales*********************************************
    
    
    function milksaleslistreport(Request $req)
    {
        $user_id = $req->post('user_id');
        



        $milksale_list = milksale::where(['user_id' => $user_id])->paginate(20);
        if (isset($milksale_list['0']->id)) {
            return response()->json([
                'status' => 200,
                'paginate_data'   => $milksale_list,
                'message' => 'Milk Sales listed sucessfully.',

            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }


    public function updatemilksalesreport(Request $req)
    {
        $data = $req->input();

        if ($req->hasFile('image')) {

            $destination_path = 'public/images/user';
            $image = $req->file('image');
            $image_name = $image->getClientOriginalName();
            $path = $req->file('image')->storeAs($destination_path, $image_name);

            $data['image'] = $image_name;
        }

        $milksale = milksale::where('id', $data['id'])->update($data);

        if ($milksale) {

            return response()->json([

                'status' => 200,
                'data'   => $data,
                'message' => 'Milk Sales  updated sucessfully.',



            ]);
        } else {

            return response()->json([

                'status' => 400,

                'message' => 'Error in updating.',


            ]);
        }
    }


    public function deleteMilksalesreport(Request $request)

    {


        $id = $request->id;
        DB::table("milksales")->whereIn('id', explode(",", $id))->delete();
        return response()->json([

            'status' => 200,

            'message' => 'Milk Sale  Deleted sucessfully.',



        ]);
    }




    // ****************************************************Milk Sales end****************************************





    // **************************************************************stockupdate***************************
    function reportstockupdateslist(Request $req)
    {
        $user_id = $req->post('user_id');


        $stockupdates_list = stockupdate::where(['user_id' => $user_id])->paginate(20);
        if (isset($stockupdates_list['0']->id)) {
            return response()->json([
                'status' => 200,
                'paginate_data'   => $stockupdates_list,
                'message' => 'stockupdates listed sucessfully.',

            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }

    function reportdesktoppayments_list(Request $req)
    {
        $user_id = $req->post('user_id');
        $credit_debit = $req->post('credit_debit');



        $payments_list = desktoppayment::where(['user_id' => $user_id])->where(['credit_debit'=>$credit_debit])->paginate(20);
        if (isset($payments_list['0']->id)) {
            return response()->json([
                'status' => 200,
                'paginate_data'   => $payments_list,
                'message' => 'Payments listed sucessfully.',

            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }



    function reportitemsalelist(Request $req)
    {
        $user_id = $req->post('user_id');


        $itemsale_list = itemsale::where(['user_id' => $user_id])->paginate(20);
        if (isset($itemsale_list['0']->id)) {
            return response()->json([
                'status' => 200,
                'paginate_data'   => $itemsale_list,
                'message' => 'Item Sale listed sucessfully.',

            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }






    function reportdispatchlist(Request $req)
    {
        $user_id = $req->post('user_id');


        $dispatch_list = dispatch::where(['user_id' => $user_id])->paginate(20);
        if (isset($dispatch_list['0']->id)) {
            return response()->json([
                'status' => 200,
                'paginate_data'   => $dispatch_list,
                'message' => 'dispatch listed sucessfully.',

            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }






    function milkvendorfilter(Request $req)
    {
        $vendor_code = $req->post('vendor_code');

        // $milkcollection->date=$req->input('date');
        // $milkcollection->time=$req->input('time');
        // // $milkcollection->fat=$req->input('fat');
        // $milkcollection->shift=$req->input('shift');
        // $milkcollection->cattle_type=$req->input('cattle_type');
        // // $milkcollection->snf=$req->input('snf');
        // $milkcollection->vendor_number=$req->input('vendor_number');
        // $milkcollection->vendor_name=$req->input('vendor_name');
        // $milkcollection->fat=$req->input('fat');
        // $milkcollection->snf=$req->input('snf');

        // $milkcollection->clr=$req->input('clr');
        // $milkcollection->rate=$req->input('rate');

        // $milkcollection->weight=$req->input('weight');
        // $milkcollection->amount=$req->input('amount');
        // $milkcollection->user_id=$req->input('user_id');

        $milkcollection_unit1 = account::where(['vendor_code' => $vendor_code])->get();
        $milkcollection_unit1->makeHidden('father');
        $milkcollection_unit1->makeHidden('address');
        $milkcollection_unit1->makeHidden('phone_number');
        $milkcollection_unit1->makeHidden('mobile_number');
        $milkcollection_unit1->makeHidden('gst_number');
        $milkcollection_unit1->makeHidden('email_address');
        $milkcollection_unit1->makeHidden('pan_number');
        $milkcollection_unit1->makeHidden('aadhaar_number');
        $milkcollection_unit1->makeHidden('bank_name');
        $milkcollection_unit1->makeHidden('city');
        $milkcollection_unit1->makeHidden('bank_branch');
        $milkcollection_unit1->makeHidden('account_number');
        $milkcollection_unit1->makeHidden('created_at');
        $milkcollection_unit1->makeHidden('updated_at');
        $milkcollection_unit1->makeHidden('deleted_at');
        $milkcollection_unit1->makeHidden('ifsc_number');
        $milkcollection_unit1->makeHidden('image');
        $milkcollection_unit1->makeHidden('id');
        $milkcollection_unit1->makeHidden('rate');
        $milkcollection_unit1->makeHidden('user_id');








        // $milkcollection_unit1->makeHidden('price');
        // $data->makeHidden('price');
        // $data->makeHidden('price');
        // $data->makeHidden('price');
        // $data->makeHidden('price');

        if (isset($milkcollection_unit1['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $milkcollection_unit1,
                'message' => 'Code Found Successfully.',

            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Invalid vendor Code'
            ]);
        }
    }




    public function reportfilter1(Request $req){


        $milkcollection = milkcollection::where('user_id', '=', $req->user_id)->where('vendor_name','=',$req->vendor_name)->where('date', '>=', $req->from)->where('date', '<=', $req->to)->get();



        if (isset($milkcollection['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $milkcollection,
                'message' => 'MilkColllection filter successfully.',

            ], 200);
        } else {
            return response()->json([
                'status' => 400,

                'message' => 'Please try again'
            ], 400);
        }



    
    }





    public function reportfiltermilksale(Request $req){


        $milksale = milksale::where('user_id', '=', $req->user_id)->where('vendor_name','=',$req->vendor_name)->where('date', '>=', $req->from)->where('date', '<=', $req->to)->get();



        if (isset($milksale['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $milksale,
                'message' => 'Milksale filter successfully.',

            ], 200);
        } else {
            return response()->json([
                'status' => 400,

                'message' => 'Please try again'
            ], 400);
        }
        }



        public function reportfilteritemsale(Request $req){


            $itemsale = itemsale::where('user_id', '=', $req->user_id)->where('name','=',$req->name)->where('date', '>=', $req->from)->where('date', '<=', $req->to)->get();
    
    
    
            if (isset($itemsale['0']->id)) {
                return response()->json([
                    'status' => 200,
                    'data'   => $itemsale,
                    'message' => 'ItemSale filter successfully.',
    
                ], 200);
            } else {
                return response()->json([
                    'status' => 400,
    
                    'message' => 'Please try again'
                ], 400);
            }


    
    }



    public function reportfilterstockupdate(Request $req){

        $start_date = Carbon::parse($req->from)->toDateTimeString();
        $end_date = Carbon::parse($req->to)->toDateTimeString();
        $stockupdate = stockupdate::where('user_id', $req->user_id)->whereBetween('created_at',[$start_date,$end_date])->get();



        if ($stockupdate->count() > 0) {
            return response()->json([
                'status' => 200,
                'data'   => $stockupdate,
                'message' => 'Stockupdate filter successfully.',

            ], 200);
        } else {
            return response()->json([
                'status' => 400,

                'message' => 'Please try again'
            ], 400);
        }



}


public function reportfilterdispatch(Request $req){


    $dispatch= dispatch::where('user_id', '=', $req->user_id)->where('dairy_code','=',$req->dairy_code)->where('date', '>=', $req->from)->where('date', '<=', $req->to)->get();



    if (isset($dispatch['0']->id)) {
        return response()->json([
            'status' => 200,
            'data'   => $dispatch,
            'message' => 'Dispatch filter successfully.',

        ], 200);
    } else {
        return response()->json([
            'status' => 400,

            'message' => 'Please try again'
        ], 400);
    }



}


    


public function reportfilterpayment(Request $req){


    $payment= desktoppayment::where('user_id', '=', $req->user_id)->where('vendor_name','=',$req->vendor_name)->where('credit_debit','=',$req->credit_debit)->where('date', '>=', $req->from)->where('date', '<=', $req->to)->get();



    if (isset($payment['0']->id)) {
        return response()->json([
            'status' => 200,
            'data'   => $payment,
            'message' => 'Dispatch filter successfully.',

        ], 200);
    } else {
        return response()->json([
            'status' => 400,

            'message' => 'Please try again'
        ], 400);
    }



}












}
