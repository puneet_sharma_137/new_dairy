<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\account;
use Validator;
use PDF;
use DB;
use PDO;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\View\View;
use App\Exports\MilkCollectionExport;
use App\Exports\MilkSaleExport;
use App\Exports\PaymentExport;
use App\Exports\StockUpdateExport;
use App\Exports\DispatchExport;
use App\Exports\DeductExport;
use App\Exports\BonusExport;
use App\Exports\ItemSalesExport;
use App\Exports\PurchaseReportExcel;
use App\Exports\SaleReportExcel;
use App\Exports\StockReportExcel;


class ExcelController extends Controller
{
    function milkcollection(Request $req)
    {
    
        return Excel::download(new MilkCollectionExport($req->id), 'MilkCollectionExport.xlsx');
        
    }
    
    function milk_sale(Request $req)
    {
    
        return Excel::download(new MilkSaleExport($req->id), 'MilkSaleExport.xlsx');
        
    }
    
    function payments(Request $req)
    {
    
        return Excel::download(new PaymentExport($req->id), 'PaymentExport.xlsx');
        
    }
    
    function stock_update(Request $req)
    {
    
        return Excel::download(new StockUpdateExport($req->id), 'StockUpdateExport.xlsx');
        
    }
    
    function dispatchExcel(Request $req) 
    {
        return Excel::download(new DispatchExport($req->id), 'DispatchExport.xlsx');  
    }
    
     function deductExcel(Request $req) 
    {
        return Excel::download(new DeductExport($req->id), 'DeductExport.xlsx');  
    }
    
     function bonusExcel(Request $req) 
    {
        return Excel::download(new BonusExport($req->id), 'BonusExport.xlsx');  
    }
    
     function itemSalesExcel(Request $req) 
    {
        return Excel::download(new ItemSalesExport($req->id), 'ItemSalesExport.xlsx');  
    }
    
    // Report Section Tab // 
    
       function purchaseReportExcel(Request $req) 
    {
        return Excel::download(new PurchaseReportExcel($req->id), 'PurchaseReportExcel.xlsx');  
    }
    
       function SaleReportExcel(Request $req) 
    {
        return Excel::download(new SaleReportExcel($req->id), 'SaleReportExcel.xlsx');  
    }
    
    function Payment(Request $req)
    {
    
        return Excel::download(new PaymentExport($req->id), 'PaymentExport.xlsx');
        
    }
    
       function StockReportExcel(Request $req) 
    {
        return Excel::download(new StockReportExcel($req->id), 'StockReportExcel.xlsx');  
    }
    
}