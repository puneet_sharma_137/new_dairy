<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use App\help_faq;
use App\help_service;
use App\help_helpline;
use App\homeimage;
use App\note;
use App\starpoint;
class AdminUserController extends Controller
{
    public function index(Request $request) {
        return view('user');
    }

    public function PaymentTransaction(Request $request) {
         if ($request->ajax()) {
            $users = DB::table('payment_details')->latest()->get();
            return Datatables::of($users)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">Edit</a>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('payment_transaction');
    }

    public function paympayment_due(Request $request) {
        return view('payment_due');
    }

    public function account(Request $request) {
        return view('account');
    }

    public function milksale(Request $request) {
        return view('milksale');
    }
    
    public function getUsersDuePayment(Request $request) {
     
     
   
        if ($request->ajax()) {
    
            
           $paymentid =   DB::table('payment_details')->select('user_id')->get();
  
         $allarylist=array();
      foreach( $paymentid as $key => $val)
        {
           $allarylist[] =  $val->user_id;
        }

          $users = DB::table('users')
                ->whereNotIn('id', $allarylist)->orderby("id","desc")
                  ->get();
    

           
            
            
           // User::latest()->get();
            return Datatables::of($users)
                    ->addIndexColumn()
                    //->addColumn('action', function($row){
                      //     $btn = '<a href="javascript:void(0)"  data-toggle="modal" data-target="#userModel"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editUser">Edit</a><a href="javascript:void(0)" data-toggle="modal" data-target="#statusModel"  data-id="'.$row->id.'" class=" btn btn-primary btn-sm mt-2 editStatus">Active/Inactive</a>';
                      //      return $btn;
                   // })
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }
    public function getUsers(Request $request) {
   
        if ($request->ajax()) {
            
            $users = User::latest()->get();
          
            return Datatables::of($users)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<a href="javascript:void(0)"  data-toggle="modal" data-target="#userModel"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editUser">Edit</a><a href="javascript:void(0)" data-toggle="modal" data-target="#statusModel"  data-id="'.$row->id.'" class=" btn btn-primary btn-sm mt-2 editStatus">Active/Inactive</a>';
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }
	
	 public function editUser($id)
    {
        $item = User::find($id);
        return response()->json($item);
    }
    
        public function updateUser(Request $request)
    {
        User::where('id' , $request->Item_id)->update(['name' => $request->name, 'lname' => $request->lname, 'email' => $request->email, 'phone_number' => $request->phone_number]);
        return response()->json(['success'=>'User updated successfully.']);
    }



        public function updateStatus(Request $request)
    {
        User::where('id' , $request->Item_id)->update([ 'isActive' => $request->is_active]);
        return response()->json(['success'=>'User updated successfully.']);
    }
    
    
    public function logoutall(Request $request) {
        Auth::logout();
        return redirect('/login');
    }
    
    
    
    
    
public function helpFaq()
{
    
    $faqlist =DB::table('help_faqs')
                ->join('users', 'help_faqs.user_id', '=', 'users.id')
                ->select('help_faqs.*','users.name', 'users.lname')
                ->orderBy("help_faqs.id","desc")->paginate(20);
  
    return view('helpFaq', compact('faqlist')); 
    //  return view('user');
  
}






    public function helpServices()
    {
        $servicelist =DB::table('help_services')
                ->join('users', 'help_services.user_id', '=', 'users.id')
                ->select('help_services.*','users.name', 'users.lname')
                ->orderBy("help_services.id","desc")->paginate(20);
     
        
        return view('helpService', compact('servicelist')); 
        
        
    }





    public function helpHelpline()
    {
        
        $helplinelist =DB::table('help_helplines')
                ->join('users', 'help_helplines.user_id', '=', 'users.id')
                ->select('help_helplines.*','users.name', 'users.lname')
                ->orderBy("help_helplines.id","desc")->paginate(20);
       
        return view('helplinelist', compact('helplinelist')); 
        
        
    }
  public function homeimageslisting()
  {
      
       $homeimages = homeimage::orderBy("id","desc")->paginate(20);
       
        return view('homeimageslist', compact('homeimages'));  
  }



    public function starpoint() {
    $points_list=starpoint::orderBy("id","desc")->paginate(20);
     return view('news',compact('points_list'));
        
    }


    public function homebanner() {
     return view('homebanner');
        
    }
    
    
    
    public function uploadimages(Request $request) 
    {
       
        
        $validatedData = $request->validate([ 
					'bannerimages' => 'required',
         ]);		 
	
		$bannerimages=trim($request->get('bannerimages'));
       
       
       if($request->hasfile('bannerimages'))
		{
			$totalimages = count($request->file('bannerimages'));
			
			if($totalimages >4)
			{
				
					
					return redirect('homebanner')->with('success', 'Cannot upload more then 4 images'); 
					die();
			}
			 if($totalimages > 0)
			{
				
				foreach($request->file('bannerimages') as $file)
				{
					 $filedata =filesize($file);
					
					if ($filedata >= 1024)
					{
					     $bytes = number_format($filedata / 1024, 2,'.','');
					}
					
					if($bytes > 1000)
					{
					 
					
						return redirect('homebanner')->with('success', 'max image size is allowed 1 mb');   
					die();
					}					
					
				}
				
			}
			
			
	
	
		if($request->hasfile('bannerimages'))
			 { 
		 
				foreach($request->file('bannerimages') as $file)
				{
					
					$name = rand(1,100000).time().'.'.$file->getClientOriginalExtension();
					$file->move(public_path().'/uploads/homeimages/', $name);  
					echo $pictures[] = $name; 
				 
				
                    $homeimg= new homeimage();
                    $homeimg->image=!empty($name)?$name:'';      
                    $homeimg->save();
				}
			 }
		
        	return redirect('homebanner')->with('success', 'Image upload Successfully');  
		}
		else {
		   	return redirect('homebanner')->with('success', 'Please select images to upload');   
		}
    }
    
    public function add_starpoints() 
    {
         return view('addstarpoint');
    }
    
    public function save_starpoints(Request $request) 
    {
       
        
        $validatedData = $request->validate([ 
		'points' => 'required',
         ]);		 
	
		$points=trim($request->get('points'));
		
		if(!empty($points))
		{
            $strobj = new starpoint();
            $strobj->points= $points;
            $strobj->save(); 
           	return redirect('add_starpoints')->with('success', 'Point Added Successfully');  
            
		}
    }
    
    
    
    
     public function destroypoints(Request $request, $id)
      {
        
         
          DB::table('starpoints')->where('id', $id)->delete();
         return back();
      }
    
      public function destroyimages(Request $request, $id)
      {
        
         
          DB::table('homeimages')->where('id', $id)->delete();
         return back();
      }
    
    

}
