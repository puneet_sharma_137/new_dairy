<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\addaccount;
use App\account;
use Validator;
use PDF;
use DB;
use PDO;
use App;
use App\Exports\BulkExport;
use App\Exports\AccountsExport;
use App\Exports\BankAccountExport;
use App\Imports\AccountImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;

// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
//sir aap user id ka relation kr skte ho model me 
class PaymentController extends Controller
{
    
     public function PaymentUpdate(Request $request)
    {
        try {
			
			$validator = Validator::make($request->all(), [
				'user_id' => 'required',
				'amount' => 'required',
				'status' => 'required',
				'payment_key' => 'required',
				
			]); 

			if ($validator->fails()) {
				$errordata =  $validator->errors()->all();
				return response(['message' => $errordata[0], 'status' => false], 422);
			} 
			
			$payment = DB::table('payment_details')->where([['user_id', $request->user_id], ['amount', $request->amount], ['status', $request->status], ['payment_key', $request->payment_key]])->latest()->first();
		
			
			if($payment) {
				return response(['message' => 'Payment Already Stored', 'status' => false]);
			}

            $payment = DB::table('payment_details')->insert([
                "user_id" => $request->user_id,
                "amount" => $request->amount,
                "status" => $request->status,
                "payment_key" => $request->payment_key
                ]);

			return response()->json(['message' => 'Payment Added','status' => true]);
			
			} catch (Exception $e) {
				return response()->json(['errors' => 'Bad Request'], 400);
			}
    }
   

}

