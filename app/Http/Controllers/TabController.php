<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\addaccount;
use App\tabdata;
use Validator;
use PDF;
use DB;
use PDO;
use App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\View\View;

class TabController extends Controller
{
    public function addTabToFavourite(Request $request) {
        $validator = Validator::make($request->all(), [
			'user_id' => 'required',
			'favourite' => 'required',
			'tab_name' => 'required',		
		]); 

		if ($validator->fails()) {
			$errordata =  $validator->errors()->all();
			return response(['message' => $errordata[0], 'status' => false], 400);
		}
        $tabExist = tabdata::where([["user_id", $request->user_id], ["tab_name", $request->tab_name]])->first();

        if($tabExist) {
            $tabExist->isFavourite = $request->favourite;
            $tabExist->save();  
        } else {
            $tab = new tabdata;
            $tab->user_id = $request->user_id;
            $tab->tab_name = $request->tab_name;
            $tab->isFavourite = $request->favourite;
            $tab->save();
        }
        if( $request->favourite == 1) {
             return response()->json(['status' => 200, 'message' => 'Tab Marked Favourite']);
        }
        return response()->json(['status' => 400, 'message' => 'Tab UnMarked from Favourite'], 400);
    }
    
    public function getFavouriteTabs(Request $request) {
        $validator = Validator::make($request->all(), [
			'user_id' => 'required',
		]); 

		if ($validator->fails()) {
			$errordata =  $validator->errors()->all();
			return response(['message' => $errordata[0], 'status' => false], 400);
		}
		
        $all = tabdata::where("user_id", $request->user_id)->get();

       
        return response()->json(['status' => 200, 'results' => $all]);
    }
    
     public function getFavouriteByTabName(Request $request) {
        $validator = Validator::make($request->all(), [
			'user_id' => 'required',
			'tab_name' => 'required',
		]); 

		if ($validator->fails()) {
			$errordata =  $validator->errors()->all();
			return response(['message' => $errordata[0], 'status' => false], 400);
		}
		
        $tab = tabdata::where([["user_id", $request->user_id], ['tab_name', $request->tab_name]])->get();
        if(!$tab || $tab->count() == 0) {
             return response()->json(['status' => 200, 'results' => []]);
        }
       
        return response()->json(['status' => 200, 'results' => $tab]);
    }
}