<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\account;
use App\milksale;
use App\itemsale;
use App\stockupdate;
use App\dispatch;
use App\deducation;
use App\bonu;
use App\milkcollection;


class DatatableController extends Controller
{
    function accountexport($id)
    {
    
      $account =  account::where('user_id',$id)->get();
      return view('datatable.index', compact('account'));
        
    }
    
    
    function bank($id)
    {
    
    
      $account =  account::where('user_id',$id)->get();
      return view('datatable.bank', compact('account'));
        
    }
    
    function milksalesexport($id)
    {
      $milksales =  milksale::where('user_id',$id)->get();
    
      return view('datatable.milksales', compact('milksales'));
    }
    
     function itemsalesexport($id)
    {
      $itemsale =  itemsale::where('user_id',$id)->get();
    
      return view('datatable.itemsales', compact('itemsale'));
    }
    
      function stockupdatesexport($id)
    {
      $stockupdate =  stockupdate::where('user_id',$id)->get();
    
      return view('datatable.stockupdates', compact('stockupdate'));
    }
    
       function dispatchesexport($id)
    {
      $dispatch =  dispatch::where('user_id',$id)->get();
    
      return view('datatable.dispatches', compact('dispatch'));
    }
    
       function deductexport($id)
    {
      $deducation =  deducation::where('user_id',$id)->get();
    
      return view('datatable.deduct', compact('deducation'));
    }
    
      function bonusexport($id)
    {
      $bonu =  bonu::where('user_id',$id)->get();
    
      return view('datatable.bonus', compact('bonu'));
    }
    
    function stockexport($id)
    {
      $stockupdate =  stockupdate::where('user_id',$id)->get();
    
      return view('datatable.stock', compact('stockupdate'));
    }
    
     function milkcollectionexport($id)
    {
      $milkcollection =  milkcollection::where('user_id',$id)->get();
    
      return view('datatable.milkcollection', compact('milkcollection'));
    }
    
    
   
    
}