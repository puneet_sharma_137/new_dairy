<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\milkcollection;
use App\milksale;
use App\itemsale;
use App\account;
use App\stockupdate;
use Validator;
use App\fat;
use App\payment;
use App\desktoppayment;
use Carbon;
use App\dispatch;
use App\deducation;
use App\bonu;
use DB;
use App\help_faq;
use App\help_service;
use App\help_helpline;




class TaskController extends Controller
{
    //
    
    
    public function addmilkcollection(Request $req)
    {
 
 
            try {
                
                $check = milkcollection::where([['date', $req->date], ['shift', $req->shift], ['vendor_number', $req->vendor_number], ['user_id', $req->user_id]])->first();
                if($check && $check->count() > 0) {
                         return response()->json([
                            'status' => 400,
                            'message' => 'Error : Milk Collection not added, shift already exists',
                        ], 400);
                }
                
                $milkcollection = new milkcollection;
                $milkcollection->date = $req->date;
                $milkcollection->time = $req->time;
                // $milkcollection->fat=$req->input('fat');
                $milkcollection->shift = $req->shift;
              
                if($milkcollection->weighing==""){
                    $milkcollection->weighing='Liter';
                }

                else{
                    //  $milkcollection->weighing='liter';
                    $milkcollection->weighing= $req->weighing;
                }
                
                
                $milkcollection->fat = $req->input('fat');
                if ($milkcollection->fat >= 0 && $milkcollection->fat <= 5) {
                    $milkcollection->cattle_type = 'cow';
                } else {
                    $milkcollection->cattle_type = 'buffalo';
                }

                
                // $milkcollection->snf=$req->input('snf');
                $milkcollection->vendor_number = $req->vendor_number;
                $milkcollection->vendor_name = $req->vendor_name;
                
                $milkcollection->snf = $req->snf;
                
                $milkcollection->clr = $req->clr;
                $milkcollection->rate = $req->rate;
                
                $milkcollection->weight = $req->weight;
                $milkcollection->amount = $req->amount;
                $milkcollection->user_id = $req->user_id;
                
                
                $milkcollection->save();
                return Response()->json([
                    'status' => 200,
                    'data' => $milkcollection,
                    'message' => 'Milk Collection Added Successfully',
                    
                ]);
            } catch (Exception $e) {
                return response()->json([
                    'status' => 400,
                    'message' => 'Error : Milk Collection not added',
                ], 400);
            }
    }
    
    
    
    
    
    public function addmilkcollectionnew(Request $req)
    {
        
        
        
        
        
        
        
        try {
            $milkcollection = new milkcollection;
            $milkcollection->date = $req->input('date');
            $milkcollection->time = $req->input('time');
            // $milkcollection->fat=$req->input('fat');
            $milkcollection->shift = $req->input('shift');
            
            $milkcollection->weighing= $req->input('weighing');
            
            
            
            
            
            
            
            $milkcollection->fat = $req->input('fat');
            if ($milkcollection->fat >= 0 && $milkcollection->fat <= 5) {
                $milkcollection->cattle_type = 'cow';
            } else {
                $milkcollection->cattle_type = 'buffalo';
            }
            
            
            
            
            
            
            $milkcollection->vendor_number = $req->input('vendor_number');
            $milkcollection->vendor_name = $req->input('vendor_name');
            
            $milkcollection->snf = $req->input('snf');
            
            $milkcollection->clr = $req->input('clr');
            $milkcollection->rate = $req->input('rate');
            
            $milkcollection->weight = $req->input('weight');
            $milkcollection->amount = $req->input('amount');
            $milkcollection->user_id = $req->input('user_id');
            
            
            $milkcollection->save();
            return Response()->json([
                
                'status' => 200,
                'data' => $milkcollection,
                'message' => 'Milk Collection Added Successfully',
                
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 400,
                'message' => 'Error : Milk Collection not added',
            ], 400);
        }
    }
    
    

    public function adddesktoppayments(Request $req)
    {
        
        
        
        
        
        
        
        try {
            $desktoppayment = new desktoppayment;
            $desktoppayment->vendor_code = $req->input('vendor_code');
            $desktoppayment->vendor_name = $req->input('vendor_name');
            $desktoppayment->date= $req->input('date');
            $desktoppayment->time= $req->input('time');
            $desktoppayment->bill_number= $req->input('bill_number');
            $desktoppayment->credit_debit = $req->input('credit_debit');
                        $desktoppayment->notes = $req->input('notes');
                        $desktoppayment->amount= $req->input('amount');



            $desktoppayment->user_id = $req->input('user_id');





            $desktoppayment->user_id = $req->input('user_id');
            
            
            $desktoppayment->save();
            return Response()->json([
                
                'status' => 200,
                'data' => $desktoppayment,
                'message' => 'Desktop Payment Added Successfully',
                
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 400,
                'message' => 'Error : Desktop Payment not added',
            ], 400);
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function milkcollectionlist(Request $req)
    {
        $user_id = $req->post('user_id');
        $shift = $req->post('shift');
        
        
        
        $milkcollection_list = milkcollection::where(['user_id' => $user_id])->where(['shift' => $shift])->paginate(20);
        if (isset($milkcollection_list['0']->id)) {
            return response()->json([
                'status' => 200,
                'paginate_data'   => $milkcollection_list,
                'message' => 'Milk Collection listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }














    public function desktoppayments_list(Request $req)
    {
        $user_id = $req->post('user_id');
        
        
        
        $desktoppayment_list = desktoppayment::where(['user_id' => $user_id])->paginate(20);
        if (isset($desktoppayment_list['0']->id)) {
            return response()->json([
                'status' => 200,
                'paginate_data'   => $desktoppayment_list,
                'message' => 'desktop payment listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }
    
    
    
    public function dislist(Request $req)
    {
       



        
        
        
        
        $milkcollection_list = milkcollection::avg('fat');
       
            return response()->json([
                'status' => 200,
                'data'   => $milkcollection_list,
                'message' => 'Milk Collection listed sucessfully.',
                
            ]);
         
    }

    public function dislist2(Request $req)
    {
       



        
        
        
        
        $milkcollection_list = milkcollection::avg('snf');
       
            return response()->json([
                'status' => 200,
                'data'   => $milkcollection_list,
                'message' => 'Milk Collection listed sucessfully.',
                
            ]);
         
    }
         
    public function dislist3(Request $req)
    {
       



        
        
        
        
        $milkcollection_list = milkcollection::avg('clr');
       
            return response()->json([
                'status' => 200,
                'data'   => $milkcollection_list,
                'message' => 'Milk Collection listed sucessfully.',
                
            ]);
         
    }
         
    
    
    
    
    public function milkcollectionlistsingle(Request $req)
    {
        $id = $req->post('id');
        
        
        $milkcollection_list_single = milkcollection::where(['id' => $id])->get();
        if (isset($milkcollection_list_single['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $milkcollection_list_single,
                'message' => 'Milk Collection listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }
    
    
    
     public function updatemilkcollection(Request $req)
    {
        $data = $req->input();
        
       
        
        $milkcollection = milkcollection::where('id', $data['id'])->update($data);
        
        if ($milkcollection) {
            
            return response()->json([
                
                'status' => 200,
                'data'   => $data,
                'message' => 'Milk Collection  updated sucessfully.',
                
                
                
            ]);
        } else {
            
            return response()->json([
                
                'status' => 400,
                
                'message' => 'Error in updating.',
                
                
            ]);
        }
    }
    
     public function deleteMilkcollection(Request $request)
    
    {
        
        
        $id = $request->id;
        DB::table("milkcollections")->whereIn('id', explode(",", $id))->delete();
        return response()->json([
            
            'status' => 200,
            
            'message' => 'Milk Collection  Deleted sucessfully.',
            
            
            
        ]);
    }
    
    
    
    
    
    
    public function unit1(Request $req)
    {
        $vendor_code = $req->post('vendor_code');
        
        
        
        $milkcollection_unit1 = account::where(['vendor_code' => $vendor_code])->get();
        if($req->user_id) {
            $milkcollection_unit1 = account::where([['vendor_code', $vendor_code], ['user_id', $req->user_id]])->get(); 
        }
       
        
        $milkcollection_unit1->makeHidden('father');
        $milkcollection_unit1->makeHidden('address');
        $milkcollection_unit1->makeHidden('phone_number');
        $milkcollection_unit1->makeHidden('mobile_number');
        $milkcollection_unit1->makeHidden('gst_number');
        $milkcollection_unit1->makeHidden('email_address');
        $milkcollection_unit1->makeHidden('pan_number');
        $milkcollection_unit1->makeHidden('aadhaar_number');
        $milkcollection_unit1->makeHidden('bank_name');
        $milkcollection_unit1->makeHidden('city');
        $milkcollection_unit1->makeHidden('bank_branch');
        
        $milkcollection_unit1->makeHidden('account_number');
        $milkcollection_unit1->makeHidden('created_at');
        $milkcollection_unit1->makeHidden('updated_at');
        $milkcollection_unit1->makeHidden('deleted_at');
        $milkcollection_unit1->makeHidden('ifsc_number');
        $milkcollection_unit1->makeHidden('image');
        
        $milkcollection_unit1->makeHidden('id');
        
        
        
        // $milkcollection_unit1->makeHidden('user_id');
        
       
        if (isset($milkcollection_unit1['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $milkcollection_unit1,
                'message' => 'Code Found Successfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Invalid vendor Code'
            ]);
        }
    }
    
    
    // *************************fat****************************************
    
    
    
    
     public function unit2(Request $req)
    {
       

        $milkcollection_unit2 = fat::where('fat', (int)$req->fat)->get();

        if ($milkcollection_unit2->count() > 0) {
            return response()->json([
                'status' => 200,
                'data'   => $milkcollection_unit2,
                'message' => 'Fat Found Successfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Invalid Fat'
            ], 400);
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
 public function milkfilter(Request $request)
    { 
        if (request()->ajax()) {
            if (!empty($request->date)) {
                $data = milkcollection::where(['date' => $date])
                ->whereBetween('date', array($request->date, $request->end_date))
                ->get();
                
                return response()->json([
                    'status' => 200,
                    'data'   => $data['0'],
                    'message' => 'List successfully.',
                    
                ], 200);
            } else {
                $data = DB::table('tbl_milkcollection')
                ->get();
            }
            return datatables()->of($data)->make(true);
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // ****************************************************MILK SALE****************************************
    
    
    public function addmilksales(Request $req)
    {
        
        
        // $validator = Validator::make($req->all(), [
            
        //     'vendor_number'=>'required',
        // ]);
        // if ($validator->fails()) {
        //     echo "Vendor Code Already Exist";
        // }
        
        // else{
            
            
            
            
            
            try {
                $milksales = new milksale;
                $milksales->date = $req->input('date');
                $milksales->time = $req->input('time');
                // $milkcollection->fat=$req->input('fat');
                $milksales->shift = $req->input('shift');
                
                $milksales->fat = $req->input('fat');
                if ($milksales->fat >= 0 && $milksales->fat <= 3) {
                    $milksales->cattle_type = 'cow';
                } else {
                    $milksales->cattle_type = 'buffalo';
                }
                
                $milksales->vendor_number = $req->input('vendor_number');
                $milksales->vendor_name = $req->input('vendor_name');
                $milksales->weight = $req->input('weight');
                
                $milksales->weighing = $req->input('weighing');
                
                
                $milksales->rate = $req->input('rate');
                
                $milksales->amount = $req->input('amount');
                $milksales->user_id = $req->input('user_id');
                
                
                
                $milksales->save();
                return Response()->json([
                    
                    'status' => 200,
                    'data' => $milksales,
                    'message' => 'Milk Sales Added Successfully'
                ]);
            } catch (Exception $e) {
                return response()->json([
                    'status' => 400,
                    'message' => 'Error : Milk Sales not added',
                ], 400);
            }
        }
    
    
     public function milksaleslist(Request $req)
    {
        $user_id = $req->post('user_id');
        $shift = $req->post('shift');
        
        $milksales_list = milksale::where(['user_id' => $user_id])->where(['shift' => $shift])->paginate(20);
        if (isset($milksales_list['0']->id)) {
            return response()->json([
                'status' => 200,
                'pagination_data'   => $milksales_list,
                'message' => 'Milk sales listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }
    
    
     public function updatemilksales(Request $req)
    {
        $data = $req->input();
        $milksales = milksale::where('id', $data['id'])->update($data);
        if ($milksales) {
            return response()->json([
                'status' => 200,
                'data'   => $data,
                'message' => 'Milk Sale Edited sucessfully.',
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Error while updating',
            ]);
        }
    }

     public function deletemilksales(Request $request)
    {
        
        $id = $request->id;
        DB::table("milksales")->whereIn('id', explode(",", $id))->delete();
        return response()->json([
            
            'status' => 200,
            
            'message' => 'Milk sales  Deleted sucessfully.',
        ]);
    }
    
    public function milksaleslistsingle(Request $req)
    {
        $id = $req->post('id');
        
        
        $milksales_list_single = milksale::where(['id' => $id])->get();
        if (isset($milksales_list_single['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $milksales_list_single,
                'message' => 'Milk Sales listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    // ****************************************************MILK SALE ENDS****************************************
    

    
    // **************************************************itemsales***********************************************
    
    
    
    
     public function additemsales(Request $request)
    {
        
        try{
                $date=$request->get('date');
                $time=$request->get('time');
                $code=$request->get('code');
                $name=$request->get('name');
                $item=$request->get('item');
                $item = implode(", ", $item);
                $units=$request->get('units');
                $units = implode(", ", $units);

                $rate=$request->get('rate');
                $quantity=$request->get('quantity');
                $amount=$request->get('amount');
                $user_id=$request->get('user_id');

                
                
                 
              if(!empty($user_id) && !empty($units) && !empty($name))
              {
                 
                    
                    $feedobj= new itemsale(); 
                    $feedobj->date=!empty($date)?$date:'';  
                    $feedobj->time=!empty($time)?$time:'';
                    $feedobj->code=!empty($code)?$code:'';
                    $feedobj->name=!empty($name)?$name:'';
                     
                    $feedobj->item=!empty($item)?$item:'';
                    $feedobj->units=!empty($units)?$units:'';
                    $feedobj->rate=!empty($rate)?$rate:'';
                    $feedobj->quantity=!empty($quantity)?$quantity:'';
                    $feedobj->amount=!empty($amount)?$amount:'';
                    $feedobj->user_id=!empty($user_id)?$user_id:'';

                   
                    $result = $feedobj->save();    
                    
                   
                    
                 
                      if($result)
                      {





                        $array_out[] = 
                        array(
                            "item" => !empty($item)?$item:'', 
                           
                        );

                      

                       $output=array( "message" => "Item Sale Added Successfull","status" => 200 );
                   
                                     print_r(json_encode($output, 200)); 
                      }
                      else
                      {
                           return response()->json(['status'=>400, 'message' => 'errr while saving data']);
                      }
                
                
               
            }
           
            
        }catch(Exception $e){
            return Response::json(['errors' => 'Bad Request'], 400);
        } 
    }



    public function itemsalelist(Request $req)
    {
        $user_id = $req->post('user_id');
       
        
        
        
        $itemsale_list =itemsale::where(['user_id' => $user_id])->paginate(20);
        if (isset($itemsale_list['0']->id)) {
            return response()->json([
                'status' => 200,
                'paginate_data'   => $itemsale_list,
                'message' => 'Item sales listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }


     public function updatedesktoppayments(Request $req)
    {
        $data = $req->input();
        
        
        $desktoppayment = desktoppayment::where('id', $data['id'])->update($data);
        
        if ($desktoppayment) {
            
            return response()->json([
                
                'status' => 200,
                'data'   => $data,
                'message' => 'Payment Edited sucessfully.',
                
                
                
            ]);
        } else {
            
            return response()->json([
                
                'status' => 400,
                'message' => 'Error while updating Payment.',
                
                
                
            ]);
        }
    }
    
    
    
    
    
    
    
    
    
    
    
     public function updatepayments(Request $req)
    {
        $data = $req->input();
        
        
        $itemsales = itemsale::where('id', $data['id'])->update($data);
        
        if ($itemsales) {
            
            return response()->json([
                
                'status' => 200,
                'data'   => $data,
                'message' => 'Profile Edited sucessfully.',
                
                
                
            ]);
        } else {
            
            return response()->json([
                
                'status' => 400,
                'message' => 'Error while updating profile.',
                
                
                
            ]);
        }
    }
    
    
    
     public function deletepayments(Request $request)
    
    {
        
        $id = $request->id;
        DB::table("desktoppayments")->whereIn('id', explode(",", $id))->delete();
        return response()->json([
            
            'status' => 200,
            
            'message' => 'Milk sales  Deleted sucessfully.',
        ]);
    }
    
    
    
    
    
     public function deletedesktoppayments(Request $request)
    
    {
        
        $id = $request->id;
        DB::table("desktoppayments")->whereIn('id', explode(",", $id))->delete();
        return response()->json([
            
            'status' => 200,
            
            'message' => 'Payment  Deleted sucessfully.',
        ]);
    }
    
    
    
    
    
    public function desktoppaymentlistsingle(Request $req)
    {
        $id = $req->post('id');
        
        
        $desktoppayment_list_single = desktoppayment::where(['id' => $id])->get();
        if (isset($desktoppayment_list_single['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $desktoppayment_list_single,
                'message' => 'Desktop Payment listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // ****************************************************End Payments******************************************
    
    
    // ****************************************************Stock Payments******************************************
    public function addstockupdates(Request $req)
    {
        
        
        try {
            $stockupdates = new stockupdate;
            $stockupdates->date = $req->input('date');
            $stockupdates->time = $req->input('time');
            
            $stockupdates->type = $req->input('type');
            $stockupdates->vendor_number = $req->input('vendor_number');
            
            $stockupdates->vendor_name = $req->input('vendor_name');
            $stockupdates->quantity = $req->input('quantity');
            $stockupdates->amount = $req->input('amount');
            $stockupdates->user_id = $req->input('user_id');
            
            
            
            
            
            
            
            $stockupdates->save();
            return Response()->json([
                
                'status' => 200,
                'data' => $stockupdates,
                'message' => 'stockupdates Added Successfully'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 400,
                'message' => 'Error : stockupdates not added',
            ], 400);
        }
    }
    public function stockupdateslist(Request $req)
    {
        $user_id = $req->post('user_id');
        
        
        $stockupdates_list = stockupdate::where(['user_id' => $user_id])->paginate(20);
        if (isset($stockupdates_list['0']->id)) {
            return response()->json([
                'status' => 200,
                'paginate_data'   => $stockupdates_list,
                'message' => 'stockupdates listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }
    
    
     public function updatestockupdates(Request $req)
    {
        $data = $req->input();
        
        
        $stockupdates = stockupdate::where('id', $data['id'])->update($data);
        
        if ($stockupdates) {
            
            return response()->json([
                
                'status' => 200,
                'data'   => $data,
                'message' => 'stockupdates Edited sucessfully.',
                
                
                
            ]);
        } else {
            
            return response()->json([
                
                'status' => 400,
                'message' => 'Error while updating stockupdates',
                
                
                
            ]);
        }
    }
    
    
    
     public function deletestockupdates(Request $request)
    
    {
        
        $id = $request->id;
        DB::table("stockupdates")->whereIn('id', explode(",", $id))->delete();
        return response()->json([
            
            'status' => 200,
            
            'message' => 'Stock Update  Deleted sucessfully.',
        ]);
    }
    
    
    
    public function stockupdatesingle(Request $req)
    {
        $id = $req->post('id');
        
        
        $desktoppayment_list_single = stockupdate::where(['id' => $id])->get();
        if (isset($desktoppayment_list_single['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $desktoppayment_list_single,
                'message' => 'Stock listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }
    
    
    // ****************************************************Stock Payments Ends******************************************
    
    // ****************************************************Dispatch******************************************
    
    
    
    
    public function adddispatch(Request $req)
    {
        
        
        try {
            $dispatch = new dispatch;
            $dispatch->date = $req->input('date');
            $dispatch->time = $req->input('time');
            $dispatch->dairy_code = $req->input('dairy_code');
            $dispatch->avg_fat = $req->input('avg_fat');
            // $dispatch->avg_fat = DB::table('milkcollections')
            //     ->avg('fat');
            $dispatch->avg_snf = $req->input('avg_snf');
            // $dispatch->avg_fat = DB::table('milkcollections')
            // ->avg('snf');
            $dispatch->avg_clr = $req->input('avg_clr');
            // $dispatch->avg_fat = DB::table('milkcollections')
            // ->avg('clr');
            $dispatch->rate = $req->input('rate');
            $dispatch->weight = $req->input('weight');
            $dispatch->no_of_cans = $req->input('no_of_cans');
            $dispatch->amount = $req->input('amount');
            $dispatch->quantity = $req->input('quantity');
            $dispatch->user_id = $req->input('user_id');
            
            
            $dispatch->save();
            return Response()->json([
                
                'status' => 200,
                'data' => $dispatch,
                'message' => 'Dispatch Added Successfully'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 400,
                'message' => 'Error : Dispatch not added',
            ], 400);
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function dispatchlist(Request $req)
    {
        $user_id = $req->post('user_id');
        
        
        $dispatch_list = dispatch::where(['user_id' => $user_id])->paginate(20);
        if (isset($dispatch_list['0']->id)) {
            return response()->json([
                'status' => 200,
                'paginate_data'   => $dispatch_list,
                'message' => 'dispatch listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }
    
    
    
    public function dispatchupdatesingle(Request $req)
    {
        $id = $req->post('id');
        
        
        $dispatch_list_single = dispatch::where(['id' => $id])->get();
        if (isset($dispatch_list_single['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $dispatch_list_single,
                'message' => 'Dispatch listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }
    
    
    
    
     public function updatedispatch(Request $req)
    {
        $data = $req->input();
        
        
        $dispatch = dispatch::where('id', $data['id'])->update($data);
        
        if ($dispatch) {
            
            return response()->json([
                
                'status' => 200,
                'data'   => $data,
                'message' => 'dispatch Edited sucessfully.',
                
                
                
            ]);
        } else {
            
            return response()->json([
                
                'status' => 400,
                'message' => 'Error while updating dispatch',
                
                
                
                
            ]);
        }
    }
    
    
    
     public function deletedispatch(Request $request)
    
    {
        
        $id = $request->id;
        DB::table("dispatches")->whereIn('id', explode(",", $id))->delete();
        return response()->json([
            
            'status' => 200,
            
            'message' => 'Dispatches  Deleted sucessfully.',
        ]);
    }
    
    
    
    
    
    
    // ****************************************************Dispatch Ends******************************************
    
    
    // ****************************************************deducations******************************************
    
    
    
    
    public function adddeducations(Request $req)
    {
        
        
        try {
            $deducations = new deducation;
            $deducations->cow_min_fat = $req->input('cow_min_fat');
            $deducations->cow_per_unit = $req->input('cow_per_unit');
            $deducations->fat_cost = $req->input('fat_cost');
            $deducations->cow_min_snf = $req->input('cow_min_snf');
            $deducations->snf_per_unit = $req->input('snf_per_unit');
            $deducations->snf_cost = $req->input('snf_cost');
            $deducations->buf_min_fat = $req->input('buf_min_fat');
            $deducations->buf_per_unit = $req->input('buf_per_unit');
            $deducations->buff_cost = $req->input('buff_cost');
            $deducations->buf_min_snf = $req->input('buf_min_snf');
            $deducations->buf_snf_per_unit = $req->input('buf_snf_per_unit');
            
            $deducations->buf_nsf_cost = $req->input('buf_nsf_cost');
            $deducations->user_id    = $req->input('user_id');
            
            
            
            
            
            
            
            
            
            
            
            
            
            $deducations->save();
            return Response()->json([
                
                'status' => 200,
                'data' => $deducations,
                'message' => 'deducations Added Successfully'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 400,
                'message' => 'Error : deducations not added',
            ], 400);
        }
    }
    public function deducationslist(Request $req)
    {
        $user_id = $req->post('user_id');
        
        
        $deducations_list = deducation::where(['user_id' => $user_id])->paginate(20);
        if (isset($deducations_list['0']->id)) {
            return response()->json([
                'status' => 200,
                'paginate_data'   => $deducations_list,
                'message' => 'deducations listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }
    
    
    
    
    public function deducationsingle(Request $req)
    {
        $id = $req->post('id');
        
        
        $deducation_list_single = deducation::where(['id' => $id])->get();
        if (isset($deducation_list_single['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $deducation_list_single,
                'message' => 'deducation listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }
    
    
    
    
     public function updatededucations(Request $req)
    {
        $data = $req->input();
        
        
        $deducations = deducation::where('id', $data['id'])->update($data);
        
        if ($deducations) {
            
            return response()->json([
                
                'status' => 200,
                'data'   => $data,
                'message' => 'deducations Edited sucessfully.',
                
                
                
            ]);
        } else {
            
            return response()->json([
                
                'status' => 400,
                'message' => 'Error while updating deducations',
                
                
                
            ]);
        }
    }
    
    
    
    
     public function deletededucations(Request $request)
    
    {
        
        $id = $request->id;
        DB::table("deducations")->whereIn('id', explode(",", $id))->delete();
        return response()->json([
            
            'status' => 200,
            
            'message' => ' Deducation  Deleted sucessfully.',
        ]);
    }
    
    
    
    
    
    
    
    // ****************************************************deducations Ends******************************************
    
    // ****************************************************Bonus*****************************************************
    
    
     public function addbonus(Request $req)
    {
        
        
        try {
            $bonus = new bonu;
            $bonus->cow = $req->input('cow');
            $bonus->cow_per_unit = $req->input('cow_per_unit');
            // $milkcollection->fat=$req->input('fat');
            $bonus->cow_cost = $req->input('cow_cost');
            
            $bonus->buff = $req->input('buff');
            
            // $milkcollection->snf=$req->input('snf');
            $bonus->buff_per_unit = $req->input('buff_per_unit');
            $bonus->buff_cost = $req->input('buff_cost');
            
            $bonus->user_id = $req->input('user_id');
            
            
            
            $bonus->save();
            return Response()->json([
                
                'status' => 200,
                'data' => $bonus,
                'message' => 'bonus Added Successfully'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 400,
                'message' => 'Error : Bonus not added',
            ], 400);
        }
    }
    
    
    
    
    public function bonuslist(Request $req)
    {
        $user_id = $req->post('user_id');
        
        
        $bonus_list = bonu::where(['user_id' => $user_id])->paginate(20);
        if (isset($bonus_list['0']->id)) {
            return response()->json([
                'status' => 200,
                'paginate_data'   => $bonus_list,
                'message' => 'Bonus listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }
    
    public function bonussingle(Request $req)
    {
        $id = $req->post('id');
        
        
        $bonus_list_single = bonu::where(['id' => $id])->get();
        if (isset($bonus_list_single['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $bonus_list_single,
                'message' => 'bonus listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }
    
    
    
    
     public function updatebonus(Request $req)
    {
        $data = $req->input();
        
        
        $bonus = bonu::where('id', $data['id'])->update($data);
        
        if ($bonus) {
            
            return response()->json([
                
                'status' => 200,
                'data'   => $data,
                'message' => 'Bonus Edited sucessfully.',
                
                
                
            ]);
        } else {
            
            return response()->json([
                
                'status' => 400,
                'message' => 'Error while updating Bonus',
                
                
                
            ]);
        }
    }
    
    
     public function deletebonus(Request $request)
    
    {
        $id = $request->id;
        DB::table("bonus")->whereIn('id', explode(",", $id))->delete();
        return response()->json([
            
            'status' => 200,
            
            'message' => 'Bonus  Deleted sucessfully.',
        ]);
    }
    
    
    
    
    
    
    
    
    // ********************************************************end bonus ****************************************************
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
     public function searchbydate(Request $req)
    {
        
        
        
        
        $milkcollection = milkcollection::where('user_id', '=', $req->user_id)->where('date', '>=', $req->from)->where('date', '<=', $req->to)->get();
        
        
        
        if (isset($milkcollection['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $milkcollection,
                'message' => 'MilkColllection filter successfully.',
                
            ], 200);
        } else {
            return response()->json([
                'status' => 400,
                
                'message' => 'Please try again'
            ], 400);
        }
    }
    
    
    
    
    
     public function milksalesfilter(Request $req)
    {
        
        
        
        
        $milksales = milksale::where('user_id', '=', $req->user_id)->where('date', '>=', $req->from)->where('date', '<=', $req->to)->get();
        
        
        
        if (isset($milksales['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $milksales,
                'message' => 'Milksales filter successfully.',
                
            ], 200);
        } else {
            return response()->json([
                'status' => 400,
                
                'message' => 'Please try again'
            ], 400);
        }
    }
    
    
    
    
    
    
     public function itemsalesfilter(Request $req)
    {
        
        
        
        
        $itemsales = itemsale::where('user_id', '=', $req->user_id)->where('date', '>=', $req->from)->where('date', '<=', $req->to)->get();
        
        
        
        if (isset($itemsales['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $itemsales,
                'message' => 'Item sales filter successfully.',
                
            ], 200);
        } else {
            return response()->json([
                'status' => 400,
                
                'message' => 'Please try again'
            ], 400);
        }
    }
    
    
    
    
    
    
     public function paymentfilter(Request $req)
    {
        
        
        
        
        $payment = desktoppayment::where('user_id', '=', $req->user_id)->where('date', '>=', $req->from)->where('date', '<=', $req->to)->get();
        
        
        
        if (isset($payment['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $payment,
                'message' => 'Payment filter successfully.',
                
            ], 200);
        } else {
            return response()->json([
                'status' => 400,
                
                'message' => 'Please try again'
            ], 400);
        }
    }
    
    
    
    
     public function stockupdatefilter(Request $req)
    {
        
        
        
        
        $stockupdate = stockupdate::where('user_id', '=', $req->user_id)->where('date', '>=', $req->from)->where('date', '<=', $req->to)->get();
        
        
        
        if (isset($stockupdate['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $stockupdate,
                'message' => 'stockupdate filter successfully.',
                
            ], 200);
        } else {
            return response()->json([
                'status' => 400,
                
                'message' => 'Please try again'
            ], 400);
        }
    }
    
    
     public function dispatchfilter(Request $req)
    {
        
        
        
        
        $dispatch = dispatch::where('user_id', '=', $req->user_id)->where('date', '>=', $req->from)->where('date', '<=', $req->to)->get();
        
        
        
        if (isset($dispatch['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $dispatch,
                'message' => 'dispatch filter successfully.',
                
            ], 200);
        } else {
            return response()->json([
                'status' => 400,
                
                'message' => 'Please try again'
            ], 400);
        }
    }
    
    
    
    
     public function deducationfilter(Request $req)
    {
        
        
        
        
        $deducation = deducation::where('user_id', '=', $req->user_id)->where('date', '>=', $req->from)->where('date', '<=', $req->to)->get();
        
        
        
        if (isset($deducation['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $deducation,
                'message' => 'deducation filter successfully.',
                
            ], 200);
        } else {
            return response()->json([
                'status' => 400,
                
                'message' => 'Please try again'
            ], 400);
        }
    }
    
    
     public function itemfilter(Request $req)
    {
        
        
        
        
        $itemsale = itemsale::where('user_id', '=', $req->user_id)->where('date', '>=', $req->from)->where('date', '<=', $req->to)->get();
        
        
        
        if (isset($itemsale['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $itemsale,
                'message' => 'Item Sale filter successfully.',
                
            ], 200);
        } else {
            return response()->json([
                'status' => 400,
                
                'message' => 'Please try again'
            ], 400);
        }
    }





// HELP SECTION


  
public function help_faq(Request $req)
{
    
    try {
            $help = new help_faq;
            $help->note = $req->note;
            $help->user_id = $req->user_id;
            $help->save();
            return Response()->json([
                
                'status' => 200,
                'data' => $help,
                'message' => 'Note Added Successfully'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 400,
                'message' => 'Error : Help Note not added',
            ], 400);
        }
    
}


public function help_faq_list(Request $req)
    {
        $user_id = $req->post('user_id');
        
        
        
        $help = help_faq::where(['user_id' => $user_id])->get();
        if (isset($help['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $help,
                'message' => 'Help Note listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }







    public function help_services(Request $req)
{
    
    try {
            $help = new help_service;
           
            $help->note = $req->input('note');
            $help->user_id = $req->input('user_id');
            
            
            
            $help->save();
            return Response()->json([
                
                'status' => 200,
                'data' => $help,
                'message' => 'Help service Note Added Successfully'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 400,
                'message' => 'Error : Help Note not added',
            ], 400);
        }
    
}


public function help_service_list(Request $req)
    {
        $user_id = $req->post('user_id');
        
        
        
        $help = help_service::where(['user_id' => $user_id])->get();
        if (isset($help['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $help,
                'message' => 'Help service Note listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }




    public function help_helpline(Request $req)
{
    
    try {
            $help = new help_helpline;
           
            $help->note = $req->input('note');
            $help->user_id = $req->input('user_id');
            
            
            
            $help->save();
            return Response()->json([
                
                'status' => 200,
                'data' => $help,
                'message' => 'Helpline Note Added Successfully'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 400,
                'message' => 'Error : Helpline Note not added',
            ], 400);
        }
    
}

public function upload_excel_file(Request $request) {
    if($request->isMethod('GET')){
         return view('upload_excel');
    } else {
        
    	    if($request->has('excel')){
                $excelName = 'excel_'. time().'.'.$request->excel->extension();  
                $request->excel->move(public_path('excel'), $excelName);
            }

            DB::table('excel')->insert([
                'user_id' => $request->id,
                'excel_url' => $excelName,
            ]);
            
        return view('upload_excel');
    }
   
}




public function excel_list(Request $request) {
    $data = DB::table('excel')->where('user_id', $request->id)->get();
    return view('excel_list', compact('data'));
}


 public function store_xl(Request $request) {
		try {
			$validator = Validator::make($request->all(), [
				'user_id' => 'required',
			
				'excel' => 'required',
			
				
			]); 

			if ($validator->fails()) {
				$errordata =  $validator->errors()->all();

				return response(['message' => $errordata[0], 'status' => false], 422);
			} 
			
				
				
				if($request->has('excel')){
                    $excelName = 'excel_'. time().'.'.$request->excel->extension();  
                    $request->excel->move(public_path('excel'), $excelName);
                }

                DB::table('excel')->insert([
                    'user_id' => $request->user_id,
                   
                    'excel_url' => $excelName,
                ]);
				return response(['message' => 'Excel has been uploaded successfully','url'=>  $excelName,'status' => true]);
			}	

			catch (Exception $e) {
				return response()->json(['errors' => 'Bad Request'], 400);
			}
				
	}
	
    public function get_xl(Request $request) {
		try {
			$validator = Validator::make($request->all(), [
				'user_id' => 'required',
				
			
				
			]); 

			if ($validator->fails()) {
				$errordata =  $validator->errors()->all();

				return response(['message' => $errordata[0], 'status' => false], 422);
			} 
			
				$excels = DB::table('excel')->where('user_id', $request->user_id)->get();
				$excelArray = [];
				
				foreach($excels as $excel){
				    array_push($excelArray, ['excel' => url('/'). '/public/excel/'.$excel->excel_url]);
				}
                $response = json_encode(['count' => count($excels), 'urls' => $excelArray ,'status' => true], JSON_UNESCAPED_SLASHES);
                return response($response );
				
			}	

			catch (Exception $e) {
				return response()->json(['errors' => 'Bad Request'], 400);
			}
				
	}
	
	
   

 public function new_tab_upload_image(Request $request) {
            return view('new_tab_upload_image');
    }


public function help_helpline_list(Request $req)
    {
        $user_id = $req->post('user_id');
        
        
        
        $help = help_helpline::where(['user_id' => $user_id])->get();
        if (isset($help['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $help,
                'message' => 'Helpline Note listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }
}