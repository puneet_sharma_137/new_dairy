<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class profile extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addprofile(Request $request)
    {
        //
       


        $request->validate([
            'name' => 'required',
        ]);

        try {
        if ($request->hasFile('file')) {

            $request->validate([
                'image' => 'mimes:jpeg,bmp,png'
            ]);
           

            
              $request->file->store('product', 'public');
              
            $profile = new profile([

                 "image" => $request->file->hashName(),
                $profile->dsc_code => $req->input('dsc_code'),
                $profile->category => $req->input('category'),
                $profile->dsc_name => $req->input('dsc_name'),
                $profile->father_name => $req->input('father_name'),
                $profile->village => $req->input('village'),
                $profile->city => $req->input('city'),
                $profile->contact => $req->input('contact'),
                $profile->acc_name => $req->input('acc_name'),
                $profile->acc_no => $req->input('acc_no'),
                $profile->bank_name => $req->input('bank_name'),
                $profile->bank_branch => $req->input('bank_branch'),
                $profile->ifsc => $req->input('ifsc'),
                $profile->pan => $req->input('pan'),
                $profile->commesion_cow => $req->input('commesion_cow'),
                $profile->commesion_buffalo => $req->input('commesion_buffalo'),
                $profile->head_load => $req->input('head_load'),
                $profile->extra_fat => $req->input('extra_fat'),
                $profile->extra_snf => $req->input('extra_snf'),
                $profile->group_profile => $req->input('group_profile'),
                $profile->rate_category => $req->input('rate_category'),
                


            ]);
            $profile->save(); // Finally, save the record.
            return response()->json([
                'status' => 200,
                'data'   => $profile,
                'message'=> 'Register successfully.', 

            ],200);
        }

       
          
        }
        catch (Exception $e) {
            //throw $th;
            return response()->json([
                'status' => 400,
                'message'=>'Error : Account not added', 
            ],400);
        }
    }

    function profilelist(Request $req){
        $user_id =$req->post('user_id');
    
    
        $profile_list=profile::where(['user_id'=>$user_id])->get();
        if(isset($profile_list['0']->id))
         {
            return response()->json([
                'status' => 200,
                'data'   => $profile_list,
                'message'=> 'Profile listed sucessfully.', 
    
            ],200);
            
         }
        else{
            return response()->json([
                'status' => 400,
                'message'=>'Oops something went wrong'
            ],400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
