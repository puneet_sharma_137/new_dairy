<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\addaccount;
use App\Account;
use Validator;
use PDF;
use DB;
use PDO;
use App;
use App\Exports\BulkExport;
use App\Exports\AccountsExport;
use App\Exports\BankAccountExport;
use App\Imports\AccountImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;

// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
//sir aap user id ka relation kr skte ho model me 
class accountController extends Controller
{
   
    function addaccount(Request $req)
    {

        $validator = Validator::make($req->all(), [
               
            'vendor_code'=>'required',
            'phone_number'=>'required',
        ]);
  
         if ($validator->fails()) {
        return response()->json([
                'status' => 400,
                'message' => 'Error : Vendor Code or Phone Number Already Exist',
            ], 400);
        }
        
        
        

    else{
        try {
            
            $check = account::where([['vendor_code', $req->vendor_code], ['user_id', $req->user_id]])->first();
            if($check) {
                return response()->json([
                    'status' => 400,
                    'message' => 'Error : Vendor Code for that user Id Already Exist',
                ], 400); 
            }
            
            $input = $req->all();

            //code...
            $account = new Account;

            // $req->validate([
                       
            //     'vendor_code' => 'required|unique:posts,vendor_code',
               
            //  ]);
            //  if ($validator->fails()) {
            //         return response()->json([
            //             'status' => false,
            //             'message'=>$validator->errors()->first(), 
            //         ],400);
            //     }

             

            if ($req->hasFile('image')) {

                $destination_path = 'public/images/account';
                $image = $req->file('image');
                $image_name = $image->hashName();
                $image_path = $req->file('image')->move($destination_path, $image_name);

                $account['image'] = $image_path;
            }else{
                $account['image'] = "";
            }


            $account->vendor_code = $req->input('vendor_code');

            $account->vendor_name = $req->input('vendor_name');
            // $req->session()->put('user',$user['user']);
            $account->father = $req->input('father');
            $account->address = $req->input('address');
            // $user->password= Hash::make($req->input('password'));
            $account->phone_number = $req->input('phone_number');

            $account->mobile_number = $req->input('mobile_number');
            $account->gst_number = ($req->input('gst_number'))?$req->input('gst_number'):"";
            $account->email_address = $req->input('email_address');
            $account->pan_number = ($req->input('pan_number'))?$req->input('pan_number'):"";
            $account->aadhaar_number = ($req->input('aadhaar_number'))?$req->input('aadhaar_number'):"";
            $account->bank_name = ($req->input('bank_name'))?$req->input('bank_name'):"";
            $account->city = ($req->input('city'))?$req->input('city'):"";


            $account->bank_branch = ($req->input('bank_branch'))?$req->input('bank_branch'):"";
            $account->account_number = ($req->input('account_number'))?$req->input('account_number'):"";
            $account->ifsc_number = ($req->input('ifsc_number'))?$req->input('ifsc_number'):"";
            $account->rate = '0';
            $account->user_id = $req->input('user_id');

            $account->save();
            
            $account = Account::where('id',$account->id)->first();
            
            return response()->json([
                'status' => 200,
                'data'   => $account,
                'message' => 'Account Added successfully.',


            ], 200);
        
        } catch (Exception $e) {
            //throw $th;
            return response()->json([
                'status' => 400,
                'message' => 'Error : Account not added',
            ], 400);
        }
    }
}


 public function add_account_upload_image(Request $request) {
            return view('add_account_upload_image');
    }



    function accountlist(Request $req)
    {
        $user_id = $req->post('user_id');
        $account_list = account::where(['user_id' => $user_id])->paginate(20);
        $newdata = $account_list;
        if (isset($account_list['0'])) {
            foreach($account_list as $k=>$val) {
                $type = DB::table('last_entry')->where(['user_id'=>$req->post('user_id'), 'vendor_id'=>$val['vendor_code']])->first();
                if($type === null) { 

                    $account_list[$k]['rate'] = '0';
                }
                else
                {
                    $account_list[$k]['rate'] = $type->type == 0?'file':$account_list[$k]['rate'];
                }
            }   
            return response()->json([
                'status' => 200,

                'paginate_data' => $account_list,
                'message' => 'Account listed sucessfully.',
            ], 200);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ], 400);
        }
    }
    
     function UploadData(Request $req)
    {
        if (request()->file('file')) {
             Excel::import(new AccountImport,request()->file('file'));
            return response()->json([
                'status' => 200,
                'message' => 'File Uploaded Successfully.',
            ], 200);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ], 400);
        }
    }




    function account_pdf_mob(Request $req)
    {
        $user_id = $req->post('user_id');
        $account_list = account::where(['user_id' => $user_id])->get();
        if (isset($account_list['0'])) {

            return response()->json([
                'status' => 200,

                'user_id' => $user_id,
                'message' => 'Pdf sucessfully created.',
                'url'=>'https://towais.com/bill/pdf_pdf'
            ], 200);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ], 400);
        }
    }







    







    function list(Request $req)
    {
        // if(auth()->check()){
    
        //     $users = account::all();
           
        //     return view('pdf')->with('users',$users);  
        //     }

        $result['data']=account::all()->where('user_id', '=', $req->user_id);
        return view('pdf',$result);
    }







    function accountlistsingle(Request $req)
    {
        $id = $req->post('id');


        $account_list_single = account::where(['id' => $id])->get();
        if (isset($account_list_single['0'])) {



            // $number = range(1,count($account_list));



            return response()->json([
                'status' => 200,
                // 'cnt'=>count($account_list),

                // $count['0'] = range(1,count($account_list)),
                // 'count'=> $count,
                'data'   => $account_list_single,
                'message' => 'Account listed sucessfully.',



            ], 200);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ], 400);
        }
    }





    // Edit accounts 




    // function update(Request $req){

    // $req->validate([
    //     'name'=>'required',
    //     'price'=>["required","numeric"],
    //    'limit'=>'required|numeric',
    //     'features'=>'required',
    //  ]);


    //     $id =$req->post('id');


    //     $account_list=account::where(['id'=>$id])->get();

    //     try {

    //    $data=account::find($req->id);
    //    $data->vendor_code=$req->vendor_code;
    //    $data->vendor_name=$req->vendor_name;
    //    $data->city=$req->city;
    //    $data->owner_name=$req->owner_name;
    //    $data->address=$req->address;
    //    $data->phone_number=$req->phone_number;
    //    $data->mobile_number=$req->mobile_number;
    //    $data->gst_number=$req->gst_number;
    //    $data->email_address=$req->email_address;
    //    $data->pan_number=$req->pan_number;
    //    $data->aadhaar_number=$req->aadhaar_number;
    //    $data->bank_branch=$req->bank_branch;
    //    $data->account_number=$req->acc_no;
    //    $data->ifsc_number=$req->ifsc_number;





    //    $data->save();
    //    return response()->json([
    //     'status' => 200,
    //     'data'   => $data,
    //     'message'=> 'Account Edited successfully.', 

    // ],200);


    // } catch (Exception $e) {
    // //throw $th;
    // return response()->json([
    //     'status' => 400,
    //     'message'=>'Error : Account not updated', 
    // ],400);
    // }

    // }

    public function updateAccount(Request $req)
    {
        $data = $req->input();

        if ($req->hasFile('image')) {

            $destination_path = 'public/images/user';
            $image = $req->file('image');
            $image_name = $image->getClientOriginalName();
            $image_path = $req->file('image')->move($destination_path, $image_name);

            $data['image'] = $image_path;
        }
        $account = account::where('id', $data['id'])->update($data);

        if ($account) {

            return response()->json([

                'status' => 200,
                'message' => 'Account updated sucessfully.',



            ]);
        } else {

            return response()->json([

                'status' => 400,

                'message' => 'Error in updating sucessfully.',


            ]);
        }
    }








    function filter1(Request $req)
    {




        $vendor_code = $req->post('vendor_code');
        $vendor_name = $req->post('vendor_name');
        $phone_number = $req->post('phone_number');



        $account_filter = account::where(['vendor_code' => $vendor_code, 'vendor_name' => $vendor_name, 'phone_number' => $phone_number])->get();
        if (isset($account_filter['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $account_filter['0'],
                'message' => 'List successfully.',

            ], 200);
        } else {
            return response()->json([
                'status' => 400,

                'message' => 'Please try again'
            ], 400);
        }

        // $phone_number =$req->post('phone_number');

        //     $password=$req->post('password');
        //     $login_user=user::where(['phone_number'=>$phone_number,'password'=>$password])->get();
        //     if(isset($login_user['0']->id))
        //      {
        //         return response()->json([
        //             'status' => 200,
        //             'data'   => $login_user,
        //             'message'=> 'Login successfully.', 

        //         ],200);

        //      }
        //     else{
        //         return response()->json([
        //             'status' => 400,
        //             'message'=>'Login Failed'
        //         ],400);
        //     }



    }


    function pdf(Request $req)
    {

        $data=account::all()->where('user_id', '=', $req->user_id);
        
        $pdf = \App::make('dompdf.wrapper');
        $pdf = PDF::loadView('pdf_pdf', ['data' => $data])->setPaper('a4', 'landscape');

        return $pdf->download('data.pdf');
    }
    
     function excelDownload(Request $req)
    {
    
        return Excel::download(new AccountsExport($req->id), 'data.xlsx');
        
    }
    
     function bankAccountCSV(Request $req)
    {
    
        return Excel::download(new BankAccountExport($req->id), 'bank_accounts.xlsx');
        
    }
    

    function pdf_mob(Request $req)
    {
        $data=account::all()->where('user_id', '=', $req->user_id);
        
        $pdf = \App::make('dompdf.wrapper');
        $pdf = PDF::loadView('pdf_pdf', ['data' => $data])->setPaper('a4', 'landscape');

        return $pdf->download('data.pdf');
    }

    function accountcsv(Request $req)
    {
        $user_id = $req->post('user_id');
        
        
        $deducations_list = account::where(['user_id' => $user_id])->get();
        if (isset($deducations_list['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $deducations_list,
                'message' => 'deducations listed sucessfully.',
                
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }




       




    }
    public function importExportView()
    {
       return view('importexport');
    }
    public function export() 
    {
        return Excel::download(new BulkExport, 'bulkData.xlsx');
        }









function excelData(Request $req){


// $data=account::all()->where('user_id', '=', $req->user_id);




    return Excel::download(new AccountExport,'accountdata.xlsx');
}



public function accountfilter(Request $req){


    $accountfilter = account::where('user_id', '=', $req->user_id)->where('vendor_name','=',$req->vendor_name)->where('phone_number','=',$req->phone_number)->get();



    if (isset($accountfilter['0']->id)) {
        return response()->json([
            'status' => 200,
            'data'   => $accountfilter,
            'message' => 'All Account filter successful.',

        ], 200);
    } else {
        return response()->json([
            'status' => 400,

            'message' => 'Please try again'
        ], 400);
    }

}

function accountAPI(Request $request) {
    $accountfilter = DB::table('accounts')->where('vendor_code', $request->code)
    ->orWhere('vendor_name', $request->name)
    ->orWhere('phone_number', $request->phone)->get();
    
    if(!$accountfilter) {
         return response()->json([
            'status' => 400,
            'message' => 'Not Found, Please try again'
        ], 400);  
    }
    
       return response()->json([
            'status' => 200,
            'result' => $accountfilter
                // 'vendor_code' => $accountfilter->vendor_code,
                //  'vendor_name' => $accountfilter->vendor_name,
                //   'address' => $accountfilter->address,
                //   'phone_number' => $accountfilter->phone_number,
                //     'mobile_number' => $accountfilter->mobile_number,
                //      'email_address' => $accountfilter->email_address,
                //       'pan_number' => $accountfilter->pan_number,
                //       'aadhaar_number' => $accountfilter->aadhaar_number,
                //       'bank_name' => $accountfilter->bank_name,
                //       'bank_branch' => $accountfilter->bank_branch,
                //       'account_number' => $accountfilter->account_number,
                //       'ifsc_number' => $accountfilter->ifsc_number,
            
        ]);
}

function deleteSelectedAccounts(Request $request) {
    if(!$request->list) {
         return response()->json([
            'status' => 400,
            'message' => 'list of ID Required'
        ], 400);
    }
    
    if($request->list) {
        $myArray = explode(',', $request->list);
            $delete = account::whereIn('id', $myArray);

        
        if($delete->delete()) {
            return response()->json([
                'status' => 200,
                'message' => 'Delete successfully'
            ]);
        }
        
          return response()->json([
                'status' => 200,
                'message' => 'Already Deleted'
            ]);
        
    }

}

function deleteSelectedBankAccounts(Request $request) {
    if(!$request->list) {
         return response()->json([
            'status' => 400,
            'message' => 'list of IDs Required'
        ], 400);
    }
    
    if($request->list) {
        $myArray = explode(',', $request->list);
            $delete = account::whereIn('user_id', $myArray);

        
        if($delete->delete()) {
            return response()->json([
                'status' => 200,
                'message' => 'Delete successfully'
            ]);
        }
        
          return response()->json([
                'status' => 200,
                'message' => 'Already Deleted'
            ]);
        
    }

}



}

// class AccountExport implements FromCollection{
//     public function  collection(){

//         $user_id = Request::get('user_id');

//         return account::where(['user_id' => $user_id])-get();
//     }
// }
