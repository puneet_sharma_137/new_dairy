<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Account; 
// use App\account; 

use App\milksale;

use App\itemsale;

use App\BuffCsv;

use App\stockupdate;

use App\dispatch;

use App\deducation;

use App\bonu;

use DB;

use Validator;

use App\milkcollection;

use App\Imports\COWExcelImport;

use App\Imports\BUFFExcelImport;

use Excel;

use App\rate;







class DataUploadController extends Controller

{

     public function NewFlatRate(Request $request)

    {

        try {

			

			$validator = Validator::make($request->all(), [

				'user_id' => 'required',

				'vendor_code' => 'required',

				'rate' => 'required',

			]);
			DB::table('last_entry')->where(['user_id'=>$request->user_id, 'vendor_id'=>$request->vendor_code])->delete();
			
			DB::table('last_entry')->insert(array(
				'type'=>1,
				'user_id'=>$request->user_id,
				'vendor_id'=>$request->vendor_code
			));

			if ($validator->fails()) {

				$errordata =  $validator->errors()->all();

				return response(['message' => $errordata[0], 'status' => false], 422);

			} 

			

			$check = DB::table('rates')->where([['user_id',$request->user_id], ['vendor_code',$request->vendor_code], ['rate',$request->rate]])->latest()->first();

		

			

			if($check) {

				return response(['message' => 'Value Already Stored', 'status' => false]);

			}

			

// 			$Updaterate = DB::table('accounts')where('rate', 1)->save();

			

// 			if (is_null($UpdateDetails)) {

//                 return false;

//                 }



           $account = Account::where([['user_id',$request->user_id], ['vendor_code',$request->vendor_code]])->first();

          

            $account->rate = $request->rate;

// 			$account->vendor_code = $request->vendor_code;

// 			$account->user_id = $request->user_id;

			$account->save();







			

			$rate = new rate;

			$rate->rate = $request->rate;

			$rate->vendor_code = $request->vendor_code;

			$rate->user_id = $request->user_id;

			$rate->save();

			

			

		

			

			return response()->json(['message' => 'New Rate Added','status' => true]);

			

        }

				catch (Exception $e) {

				return response()->json(['errors' => 'Bad Request'], 400);

			}

    }

    

     public function LatestRate(Request $request)

    {

        try {

			

			$validator = Validator::make($request->all(), [

				'user_id' => 'required',

				'vendor_code' => 'required',

			]); 



			if ($validator->fails()) {

				$errordata =  $validator->errors()->all();

				return response(['message' => $errordata[0], 'status' => false], 422);

			}

			

			

			$rate = rate::where([['user_id', $request->user_id], ['vendor_code', $request->vendor_code]])->latest()->first();

			$assignedVendor = DB::table('assign_file_scheme')->where([['vendor_code', $request->vendor_code], ['user_id', $request->user_id]])->latest()->first();

			$type = DB::table('last_entry')->where(['user_id'=>$request->user_id, 'vendor_id'=>$request->vendor_code])->first();
			if(empty($type)) {
				return response()->json(['message' => 'No File or Flat Rate Assigned']);
			}
			if($type->type == 0) {

				//  return response()->json(['message' => 'Flat Rate Assigned', 'value' => $rateLatest->rate]); 

				  return response()->json(['message' => 'File Rate Assigned']); 

			   } else {

					return response()->json(['message' => 'Flat Scheme Assigned']); 

			   }
			// if($rate && !$assignedVendor) {

			//     return response()->json(['message' => 'Only Flat Rate Assigned']);

			// } else if(!$rate && $assignedVendor) {

			//      return response()->json(['message' => 'Only File Scheme Assigned']);

			// } else if(($rate && $assignedVendor)) {

			//     $rateLatest = rate::where([['user_id', $request->user_id], ['vendor_code', $request->vendor_code]])->latest()->first();

			//   //dd($rateLatest);

			//    // dd(strtotime($rateLatest->updated_at));

			//     if(strtotime($rateLatest->updated_at) > strtotime($assignedVendor->updated_at)) {

			//      //  return response()->json(['message' => 'Flat Rate Assigned', 'value' => $rateLatest->rate]); 

			//        return response()->json(['message' => 'Flat Rate Assigned']); 

			//     } else {

			//          return response()->json(['message' => 'File Scheme Assigned']); 

			//     }

			// }

			

				return response()->json(['message' => 'Not Found'], 400);

			

        }

        	catch (Exception $e) {

				return response()->json(['errors' => 'Bad Request'], 400);

			}

        

    }

    

     public function CowUploadFile(Request $request, $id)

    {

        Excel::import(new COWExcelImport($id), request()->file('fileToUpload'));

        

        return response()->json(['success' => 'Excel Imported, Download to see the imported data.']);

    }

    

    public function CheckFlatRate(Request $request)

    {

        try {

			

			$validator = Validator::make($request->all(), [

				'user_id' => 'required',

				'vendor_code' => 'required',

			]); 



			if ($validator->fails()) {

				$errordata =  $validator->errors()->all();

				return response(['message' => $errordata[0], 'status' => false], 422);

			}

			

				$rate = rate::where([['user_id', $request->user_id], ['vendor_code', $request->vendor_code]])->latest()->first();

				

				 if($rate )  {

			       return response()->json(['message' => 'Flat Rate Assigned', 'value' => $rate->rate]); 

			    }else {

                          return response()->json(['message' => 'Flat Rate Not Assigned yet', 'status' => false]); 

                    }

        

				// return response()->json(['message' => 'Not Found'], 400);

			

        }

        	catch (Exception $e) {

				return response()->json(['errors' => 'Bad Request'], 400);

			}

        

    }

    

     public function CheckFileRate(Request $request)

    {

        try {

			

			$validator = Validator::make($request->all(), [

				'user_id' => 'required',

				'vendor_code' => 'required',

			]); 



			if ($validator->fails()) {

				$errordata =  $validator->errors()->all();

				return response(['message' => $errordata[0], 'status' => false], 422);

			}

			

				// $rate = rate::where([['user_id', $request->user_id], ['vendor_code', $request->vendor_code]])->latest()->first();

				

				$users = DB::table('assign_file_scheme')

				// ->select('assign_file_scheme.Name','user.Age','user.FatherGotra','user.Profession','user.City','user.State','user.AreYouManglik','user.DOB','user.ProfilePic','user.id','shortlist.isShortlisted as isshortlist')

                    ->join('buff_csv', 'assign_file_scheme.file_id', 'buff_csv.file_id')

                    

                    ->where('assign_file_scheme.user_id',$request->user_id)

                    ->where('assign_file_scheme.vendor_code',$request->vendor_code)

                    ->get();

				// dd($users);

				 if($users)  {

			       return response()->json(['message' => 'Flat Rate Assigned', 'value' => $users]); 

			    }

        

				// return response()->json(['message' => 'Not Found'], 400);

			

        }

        	catch (Exception $e) {

				return response()->json(['errors' => 'Bad Request'], 400);

			}

        

    }

    

     public function CowUploadview(Request $request, $id)

    {

        return view('CowUploadview', compact('id'));

    }

    

    public function BuffUploadview(Request $request, $id)

    {

        return view('BuffUploadview', compact('id'));

    }

    

    

    public function BuffUploadFile(Request $request, $id)

    {
        date_default_timezone_set('Asia/Kolkata');
//echo date('d-m-Y H:i');

        $excelName = date('d_m_Y_His').'.'.$request->fileToUpload->extension();

        $request->file_title = $excelName;

     

        

        Excel::import(new BUFFExcelImport($request), request()->file('fileToUpload'));

      

        $request->fileToUpload->move(public_path('buff_excel'), $excelName);

        

        return response()->json(['success' => 'Excel Imported, Download to see the imported data.']);

    }

    

    

    

     public function BuffDeleteFile(Request $request)

    {

      

        $delete = BuffCsv::where([['user_id', $request->user_id], ['file_id', $request->file_id]])->delete();

        if($delete) {

        return response()->json(['success' => 'File Deleted','status' => 'true']);

        }

        

        return response()->json(['success' => 'File not found','status' => 'false']);

    }

    

    

     public function FileList(Request $request)

    {

      try{

			$results  = [

				'results' => BuffCsv::all()

			];

			return response()->json($results);

		}

		catch (Exception $e) {

			return response()->json(['errors' => 'Bad Request'], 400);

		}

    }

    

    

    

     public function FileAssign(Request $request)

    {

      

        try {

			

			$validator = Validator::make($request->all(), [

				'user_id' => 'required',

				'vendor_code' => 'required',

				'file_id' => 'required',

			]); 



			if ($validator->fails()) {

				$errordata =  $validator->errors()->all();

				return response(['message' => $errordata[0], 'status' => false], 422);

			} 

			

			$file = BuffCsv::where([['user_id', $request->user_id],['file_id', $request->file_id]])->first();

			if($file) {

    		    $assign = DB::table('assign_file_scheme')->insert([

    		            'file_id' => $file->file_id,

    		            'user_id' => $file->user_id,

    		            'vendor_code' => $request->vendor_code,

    		        ]);
					DB::table('last_entry')->where(['user_id'=>$request->user_id, 'vendor_id'=>$request->vendor_code])->delete();
			
					DB::table('last_entry')->where('user_id',$request->user_id)->insert(array(
						'type'=>0,
						'user_id'=>$request->user_id,
						'vendor_id'=>$request->vendor_code
					));


    		        if($assign) {

    		          	return response()->json(['success' => 'File Assigned Successfully','status' => 'true']);  

    		        }

    		        return response()->json(['errors' => 'File Not Assigned'], 400);	

			} 

			

			 return response()->json(['errors' => 'File Not Found','status' => 'false'], 400);

			

        }

				catch (Exception $e) {

				return response()->json(['errors' => 'Bad Request'], 400);

			}

    }

    

    

     public function GetDataValue(Request $request)

    {

        try {

			

			$validator = Validator::make($request->all(), [

				'snf' => 'required',

				'fat' => 'required',

				'user_id' => 'required',

				'vendor_code' => 'required'

			]); 



			if ($validator->fails()) {

				$errordata =  $validator->errors()->all();

				return response(['message' => $errordata[0], 'status' => false], 422);

			} 

			

			 $assignedVendor = DB::table('assign_file_scheme')->where([['vendor_code', $request->vendor_code], ['user_id', $request->user_id]])->orderBy('id','desc')->first();

	        

			        if(!$assignedVendor) {

			            return response()->json(['value' => Null,'message' => 'Vendor File Not Found','status' => false], 400); 

			        }

			

			
                
			       // Seq is row of excel

				$excel = BuffCsv::where([['user_id', $request->user_id], ['0',$request->fat], ['file_id', $assignedVendor->file_id] ])->first();
                
				// $snfNumber = null;

				// for($i = 1; $i <=22; $i++){

				//     if( BuffCsv::where([['user_id', $request->user_id], [$i,$request->snf], ['file_id', $assignedVendor->file_id] ])->exists()){

				//         $snfNumber = $i;

				//         break;

				//     }

				// }

			

    			if(!$excel) {

    			    return response()->json(['value' => Null,'message' => 'Bad Request, Not Found','status' => false], 400);

    			}

    			
               
    			//$value = $excel[$snfNumber];
    			$snf = str_replace('.', '_', $request->snf);
                $value = $excel[$snf];    
    			

    			if(!$value) {

    			    return response()->json(['value' => Null,'message' => 'Incorrect Column Number, doesnt exists','status' => false], 400);

    			}


				return response()->json(['value' => $value,'message' => 'Value found','status' => true]);

        }

				

		catch (Exception $e) {

			return response()->json(['value' => Null,'errors' => 'Bad Request'], 400);

		}

    

    }

    

     public function get_uploded_file(Request $request) {

		try {

			

			$validator = Validator::make($request->all(), [

				'user_id' => 'required',

			]); 



			if ($validator->fails()) {

				$errordata =  $validator->errors()->all();

				return response(['message' => $errordata[0], 'status' => false], 422);

			} 

			

				$files = DB::table('buff_csv')->where('user_id', $request->user_id)->orderBy('id', 'desc')->get();

				$fileArray = [];

				$fileIds = [];

				foreach($files as $file){
				    

				    $exists = in_array($file->file_id, $fileIds, TRUE);
                    
				    

				    if(!$exists) {
                        
				        array_push($fileIds, $file->file_id);
                        
    				    array_push($fileArray, [

    				    "file_id" => $file->file_id,

    				    // 'file' => url('/'). '/public/buff_file/'.$file->file_url,
    				    'file' => url('/'). '/public/buff_excel/'.$file->file_url,

    				     "file_type" => $file->file_type,

    				     "file_title" => $file->file_title,

    				    ]);
    				    
    				    

				    }

				}
                
                $fileArray = array_slice($fileArray, 0, 5);
			
				return response()->json(['count' => count($fileArray), 'urls' => $fileArray ,'status' => true],200);
                // $response = json_encode(['count' => count($fileIds), 'urls' => $fileArray ,'status' => true], JSON_UNESCAPED_SLASHES);

                // return response($response );

				

			}	

			catch (Exception $e) {

				return response()->json(['errors' => 'Bad Request'], 400);

			}

				

	}

    

    

    

}