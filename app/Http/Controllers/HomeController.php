<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\note;
use App\starpoint;
use App\homeimage;
use Illuminate\Pagination\Paginator;

class HomeController extends Controller
{
    //

    function addnote(Request $req){


        try{
            $note=new note;
            $note->note=$req->input('note');

            $note->user_id=$req->input('user_id');
           
        
            
            $note->save();
            return Response()->json([
        
                'status'=>200,
                'data'=>$note,
                'message'=>'Note Added Successfully'
            ]);
        
           
        
        
        
        }
        catch(Exception $e){
            return response()->json([
                'status' => 400,
                'message'=>'Error : Note not added', 
            ],400);
        }
        
        


}


function notelist(Request $req){
    $user_id =$req->post('user_id');
   $note_list=note::where('user_id', $user_id)->get();
    //   $note_list=note::where(['user_id'=>$user_id])->paginate();
    //   $note_list=note::where(['user_id'=>$user_id]);
    
    // if(isset($note_list['0']->id))
      if(isset($note_list['0']->id))
     {
        return response()->json([
            'status' => 200,
            'data'   => $note_list,
            'message'=> 'user notes listed sucessfully.', 

        ],200);
        
     }
    else{
        return response()->json([
            'status' => 400,
            'message'=>'Oops something went wrong'
        ],400);
    }
}



function addstarpoints(Request $req){


    try{
        $starpoint=new starpoint;
        $starpoint->points=$req->input('points');
        $starpoint->user_id=$req->input('user_id');

       
    
        
        $starpoint->save();
        return Response()->json([
    
            'status'=>200,
            'data'=>$starpoint,
            'message'=>'Point Added Successfully'
        ]);
    
       
    
    
    
    }
    catch(Exception $e){
        return response()->json([
            'status' => 400,
            'message'=>'Error : Point not added', 
        ],400);
    }
    
    


}


function starpointslist(Request $req){
    $points_list=starpoint::all();
if(isset($points_list['0']->id))
 {
    return response()->json([
        'status' => 200,
        'data'   => $points_list,
        'message'=> 'star point listed sucessfully.', 

    ],200);
    
 }
else{
    return response()->json([
        'status' => 400,
        'message'=>'Oops something went wrong'
    ],400);
}
}






function addhomeimages(Request $req){

        
    //code...
 
    try {
      
       

            
                //code...
                $homeimages = new homeimage;
                                    //   "image" = $req->file->hashName(),
                // $profile->image=$req->input('image')->hashName();


                if($req->hasFile('image')){


                    

                    $destination_path='public/homeimages';
                    $image=$req->file('image');
                    $image_name=$image->hashName();
                    $image_path=$req->file('image')->move($destination_path,$image_name);
    
                    $homeimages['image']=$image_path;
                   }



               
        
        $homeimages->save(); // Finally, save the record.
        
        return response()->json([
            'status' => 200,
            'data'   => $homeimages,
            'message'=> 'Image Added successfully.', 

        ],200);
    }




      
    
    catch (Exception $e) {
        //throw $th;
        return response()->json([
            'status' => 400,
            'message'=>'Error : Image not added', 
        ],400);
    }
}

function homeimageslist(Request $req){
    
    $image_list=homeimage::all();
    
    
    // $image_list=homeimage::where(['user_id'=>$user_id])->get();
    if(isset($image_list['0']->id))
     {
        return response()->json([
            'status' => 200,
            'data'   => $image_list,
            'mainurl'   =>url('/'). "/public/uploads/homeimages/",
            'message'=> 'Home images listed sucessfully.', 
    
        ],200);
        
     }
    else{
        return response()->json([
            'status' => 400,
            'message'=>'Oops something went wrong'
        ],400);
    }
    }





}
