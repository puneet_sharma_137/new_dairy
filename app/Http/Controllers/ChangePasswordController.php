<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\UpdatePasswordRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Hash;



class ChangePasswordController extends Controller
{
    //

    public function passwordResetProcess(UpdatePasswordRequest $request){
        return $this->updatePasswordRow($request)->count() > 0 ? $this->resetPassword($request) : $this->tokenNotFoundError();
        
      }
  
      // Verify if token is valid
      private function updatePasswordRow($request){
         return DB::table('password_resets')->where([
             'email' => $request->email,
             'token' => $request->resetToken
         ]);
      }
  
      // Token not found response  
      private function tokenNotFoundError() {
          return response()->json([
            'status' => 400,
            'message'=> 'Either your email or token is wrong.', 
          ],Response::HTTP_UNPROCESSABLE_ENTITY);
      }
  
      // Reset password
      private function resetPassword($request) {
          // find email
          $userData = User::whereEmail($request->email)->first();
          // update password
          $userData->update([
            'password'=>$request->password
          ]);
          // remove verification data from db
          $this->updatePasswordRow($request)->delete();
  
          // reset password response
          return response()->json([
            
            'status' => 200,
            'message'=> 'Password has been updated.', 
          ],Response::HTTP_CREATED);
     }
     
      public function password(Request $request){
            
            if(!($request->current_password && $request->new_password && $request->new_password_confirmation)) {
                return response()->json([
                    'status' => 400,
                    'message' => "Required Fields: user_id, current_password, new_password, new_password_confirmation"], 400); 
            }
            
            $user = User::find($request->user_id);
            if(!$user) {
                  return response()->json([
                    'status' => 400,
                    'message' => "User not found"], 400); 
            }
            if (!($request->get('current_password') == $user->password)) {
                // The passwords matches
                return response()->json([
                    'status' => 400,
                    'message' => "Your current password does not matches with the password you provided. Please try again."], 400);
            }
            if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
                //Current password and new password are same
                return response()->json([
                    'status' => 400,
                    'message' => "New Password cannot be same as your current password. Please choose a different password."], 400);
            }
            if($request->get('new_password') != $request->get('new_password_confirmation')){
                //new password and confirm password are different
                  return response()->json([
                    'status' => 400,
                    'message' => "Your New Password does not matches with Confirm Password"], 400);
            }
   
          
            //Change Password
            $user->password = $request->get('new_password');
            $user->save();
            return response()->json([
                    'status' => 200,
                    'message' => "Password changed successfully!"]);
    }
}
