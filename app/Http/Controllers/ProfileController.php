<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\profile;
use Storage;
use hash;
use Auth;
use App\Http\Request2;
//including post model to controller
use validate;
use App\Post;
use App\Document;
use App\Category;
//if we want to use sql syntax for queries 
use DB; 
use File;
use Image;
use Mail;
use Session;
USE Validator;
use Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\UploadedFile;
use App\Http\Resources\UserProfileResource;

class ProfileController extends Controller
{
    //


    function addprofile(Request $req){

        
            //code...
         
            try
            {
                $input=$req->all();
                $check = profile::where('dsc_code', $req->dsc_code)->first();
                if($check) {
                     return response()->json(['status' => 400, 'message'=> 'DSC Code is already Exists'], 400);
                }
                //code...
                $profile = new profile;
                                
                  if($req->hasFile('image'))
                {

                    $destination_path='public/images/user';
                    $image=$req->file('image');
                    $image_name=$image->hashName();
                    $image_path=$req->file('image')->move($destination_path,$image_name);
    
                    $profile['image']=$image_path;
                }



                $profile->dsc_code=$req->input('dsc_code'); // uniq need 
                $profile->category = $req->input('category');
                $profile->address = $req->input('address');

                $profile->dsc_name = $req->input('dsc_name');
                $profile->father_name = $req->input('father_name');
                $profile->village = $req->input('village');
                $profile->city = $req->input('city');
                $profile->contact = $req->input('contact');
                $profile->acc_name = $req->input('acc_name');
                $profile->acc_no = $req->input('acc_no');
                $profile->bank_name = $req->input('bank_name');
                $profile->bank_branch = $req->input('bank_branch');
                $profile->ifsc = $req->input('ifsc');
                $profile->pan = $req->input('pan');
                $profile->user_id=$req->input('user_id');
            

        
                $profile->save(); // Finally, save the record.
                
                return response()->json([
                    'status' => 200,
                    'data'   => $profile,
                    'message'=> 'Account Added successfully.', 
    
                ],200);
            }

            catch (Exception $e) {
                //throw $th;
                return response()->json([
                    'status' => 400,
                    'message'=>'Error : Account not added', 
                ],400);
            }
        }


        function addprofile2(Request $req, $id){
            //code...
            
            $image_name = null;
                        //code...
                          // dd($id);
                        $profile = profile::where('user_id', $id)->first();
                          
                        if($req->hasFile('image') && $profile && $req->isMethod('POST')){
                          
                            $destination_path='public/images/user';
                            
                            $image=$req->file('image');
                            $image_name=$image->hashName();
                            $image_path=$req->file('image')->move($destination_path,$image_name);
                            
                            $profile->image = $image_path;
                            $profile->save();
                           }
                          
                
               // Finally, save the record.
                $uploadedImage = null;
                if($image_name) {
                      $uploadedImage = $image_name; 
                }
             
                return view('upload_image', compact('uploadedImage'));
        }
    
    function profilelist(Request $req){
     if(!$req->user_id) {
         return response()->json(['status' => 400, 'message'=> 'user_id field is required'], 400);
     }
        $profile_list= profile::where('user_id', $req->user_id)->first();

       //  $profile_list= DB::table('profiles')->join('profile','profile.user_id','=','profiles.user_id')->where('profile.user_id', $req->user_id)->orderBy('profile.id', 'ASC')->first();
    
    
    if($profile_list)
         {
            return response()->json([
                'status' => 200,
                'data'   => [
                    "user_id" => $profile_list->user_id,
                    "dsc_code"  => $profile_list->dsc_code,
                    "category"  => $profile_list->category,
                    "address"  => $profile_list->address,
                    "dsc_name"  => $profile_list->dsc_name,
                    "father_name"  => $profile_list->father_name,
                    "vehicle"  => $profile_list->vehicle,
                    "village" => $profile_list->village,
                    "city" => $profile_list->city,
                    "contact" => $profile_list->contact,
                    "acc_name" => $profile_list->acc_name,
                    "acc_no" => $profile_list->acc_no,
                    "bank_name" => $profile_list->bank_name,
                    "bank_branch" => $profile_list->bank_branch,
                    "ifsc" => $profile_list->ifsc,
                    "pan" => $profile_list->pan,
                    "profile_image_url" => url('/').'/'.$profile_list->image
                    ],
                'message'=> 'Profile listed sucessfully.', 
    
            ],200);

        // if($profile_list)
        //  {
        //     return response()->json([
        //         'status' => 200,
        //         'data'   => [
        //             $profile_list,
        //             "profile_image_url" => url('/') .'/'.$profile_list->profile_url
        //             ],
        //         'message'=> 'Profile listed sucessfully.', 
    
        //     ],200);
        
        // user_id
        //             dsc_code
        //             category
        //             address
        //             dsc_name
        //             father_name
        //             vehicle
        //             village
        //             city
        //             contact
        //             acc_name
        //             acc_no
        //             bank_name
        //             bank_branch
        //             ifsc
        //             pan
            
         }
        else{
            return response()->json([
                'status' => 400,
                'message'=>'No Profile Added'
            ],400);
        }
    }
    


















    public function updateProfile(Request $req)
    {
        $data = $req->input();

        if($req->hasFile('image')){

            $destination_path='public/profile';
            $image=$req->file('image');
            $image_name=$image->hashName();
            $image_path=$req->file('image')->move($destination_path,$image_name);

            $data['image']=$image_path;


           

           }
        $profile = profile::where('user_id', $req->user_id)->first()->update($data);
        
if ($profile) {

                return response()->json([

                    'status' => 200,
                'data'   => $data,
                'message'=> 'Profile Edited sucessfully.', 

                

                ]);

            } else {

                return response()->json([

                    'status' => 400,
                'message'=> 'Error while updating profile.', 

                    

                ]);

            }
        
    }

 public function store_profile(Request $request) {
		try {
			$validator = Validator::make($request->all(), [
				'user_id' => 'required',
				'profile' => 'required',
			]); 

			if ($validator->fails()) {
				$errordata =  $validator->errors()->all();
				return response(['message' => $errordata[0], 'status' => false], 422);
			} 
			
				if($request->has('profile')){
                    $profileName = 'profile_'. time().'.'.$request->profile->extension();  
                    $request->profile->move(public_path('profile'), $profileName);
                }

                DB::table('profile')->insert([
                    'user_id' => $request->user_id,
                    'profile_url' => $profileName,
                ]);
				return response(['message' => 'profile has been uploaded successfully','url'=>  $profileName,'status' => true]);
			}	

			catch (Exception $e) {
				return response()->json(['errors' => 'Bad Request'], 400);
			}
				
	}
	
    public function get_profile(Request $request) {
		try {
			$validator = Validator::make($request->all(), [
				'user_id' => 'required',
				
			]); 

			if ($validator->fails()) {
				$errordata =  $validator->errors()->all();

				return response(['message' => $errordata[0], 'status' => false], 422);
			} 
			
				$profiles = DB::table('profile')->where('user_id', $request->user_id)->get();
				$profileArray = [];
				
				foreach($profiles as $profile){
				    array_push($profileArray, url('/'). '/public/profile/'.$profile->profile_url);
				}
                $response = json_encode(['count' => count($profiles), 'urls' => $profileArray ,'status' => true], JSON_UNESCAPED_SLASHES);
                return response($response );
				
			}	

			catch (Exception $e) {
				return response()->json(['errors' => 'Bad Request'], 400);
			}
				
	}













    public function pdateProfile(Request $request)

    {

        $rules = [

             'image'=>'required',
            'dsc_code'    =>  '0',

            'category'  =>     'required|min:1',

            'address'    =>  'required',

            'dsc_code'  =>  'required',

            'father_name' =>  'required',
            'village' =>  'required',
            'city' =>  'required',
            'contact' =>  'required',
            'acc_name' =>  'required',
            'acc_no' =>  'required',
            'bank_name' =>  'required',
            'bank_branch' =>  'required',
            'ifsc' =>  'required',
            'pan' =>  'required'

        ];

        $validator = Validator::make($request->all(), $rules);

        if (!$validator->passes()) {

            $errors = implode(",", $validator->errors()->all());

            return response()->json([

                'resCode'   =>  200,

                'successs'  =>  false,

                'message'   =>  $errors,

                'data'      =>  [],

                'cnt'       =>  0

            ]);

        } else {

            $updateProfile = profile::where('id', $request->input('id'))->update($request->all());

            if (!$updateProfile) {

                return response()->json([

                    'resCode'   =>  200,

                    'success'   =>  false,

                    'message'   =>  'Server error, please try again later',

                    'data'      =>  [],

                    'cnt'       =>  0

                ]);

            } else {

                $Profile = profile::where('id', $request->input('id'))->get()->toArray();

                $image = array();

                // $profileImage = json_decode($Profile[0]['image']);

                // foreach ($profileImage as $image) {

                    // $images[] = asset($image);


                if($request->hasFile('image')){

                    $destination_path='public/images/user';
                    $image=$request->file('image');
                    $image_name=$image->getClientOriginalName();
                    $path=$request->file('image')->storeAs($destination_path,$image_name);
    
                    $profile['image']=$image_name;
                   }
                    // $image_name->save();
                                    $profile[0]['image'] = $image;



               
                    return response()->json([

                    'resCode'       =>      200,

                    'success'       =>      true,

                    'messsage'      =>      'Account Updated successfully',

                    'data'          =>      $Profile['0'],


                ]);
            }

            }
        }
    }

        


        // *********************************************************************************
       

        // **********************************************************************************






    




