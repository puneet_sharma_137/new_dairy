<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\addaccount;
use App\account;
use Validator;
use PDF;
use DB;
use PDO;
use App;
use App\AnalyzerData;
use App\WeighingData;
use App\Exports\BulkExport;
use App\Exports\AccountsExport;
use App\Exports\BankAccountExport;
use App\Imports\AccountImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;

// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
//sir aap user id ka relation kr skte ho model me 
class AnalyzerDataController extends Controller
{
    
     public function AnalyzerData(Request $request)
    {
        try {
			
			$validator = Validator::make($request->all(), [
				'user_id' => 'required',
				'port' => 'required',
				'scan' => 'required',
				'value' => 'required',
				
			]); 

			if ($validator->fails()) {
				$errordata =  $validator->errors()->all();
				return response(['message' => $errordata[0], 'status' => false], 422);
			} 
			
			$payment = DB::table('analyzer_data')->where([['user_id', $request->user_id], ['port', $request->port], ['scan', $request->scan], ['value', $request->value]])->latest()->first();
		
			
			if($payment) {
				return response(['message' => 'value Already Stored', 'status' => false]);
			}

            $payment = DB::table('analyzer_data')->insert([
                "user_id" => $request->user_id,
                "port" => $request->port,
                "scan" => $request->scan,
                "value" => $request->value
                ]);

			return response()->json(['message' => 'Data Added','status' => true, 'status' => true ]);
			
			} catch (Exception $e) {
				return response()->json(['errors' => 'Bad Request'], 400);
			}
    }
    
    
    public function AnalyzerDataList(Request $request)
    {
      try{
			$results  = [
				'results' => AnalyzerData::all()
			];
			return response()->json($results);
		}
		catch (Exception $e) {
			return response()->json(['errors' => 'Bad Request'], 400);
		}
    }
    
    
    
    public function WeighingDataList(Request $request)
    {
      try{
			$results  = [
				'results' => WeighingData::all()
			];
			return response()->json($results);
		}
		catch (Exception $e) {
			return response()->json(['errors' => 'Bad Request'], 400);
		}
    }
    
    // WeighingData
    
     public function WeighingData(Request $request)
    {
        try {
			
			$validator = Validator::make($request->all(), [
				'user_id' => 'required',
				'port' => 'required',
				'scan' => 'required',
				
				
			]); 

			if ($validator->fails()) {
				$errordata =  $validator->errors()->all();
				return response(['message' => $errordata[0], 'status' => false], 422);
			} 
			
			$payment = DB::table('weighing_data')->where([['user_id', $request->user_id], ['port', $request->port], ['scan', $request->scan]])->latest()->first();
		
			
			if($payment) {
				return response(['message' => 'value Already Stored', 'status' => false]);
			}

            $payment = DB::table('weighing_data')->insert([
                "user_id" => $request->user_id,
                "port" => $request->port,
                "scan" => $request->scan
                
                ]);

			return response()->json(['message' => 'Data Added','status' => true, 'status' => true ]);
			
			} catch (Exception $e) {
				return response()->json(['errors' => 'Bad Request'], 400);
			}
    }
    
   

}

