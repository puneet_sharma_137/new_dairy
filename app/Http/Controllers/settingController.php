<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\weight;
use App\analyzer_data;
use App\user_analyzer_data;
use App\setting;
use App\analyzer_setting;




class settingController extends Controller
{
    //

    public function weight(Request $req){

        try{
            $weight=new weight;
           
            $weight->weight=$req->input('weight');
            $weight->user_id=$req->input('user_id');

            
        
            
            $weight->save();
            return Response()->json([
        
                'status'=>200,
                'data'=>$weight,
                'message'=>'Weight Added Successfully',
        
            ]);
        
           
        
        
        
        }       
        catch(Exception $e){
            return response()->json([
                'status' => 400,
                'message'=>'Error :Weight not added', 
            ],400);
        }

    }

    function getweight(Request $req)
    {
        $user_id = $req->post('user_id');


        $getweight= weight::where(['user_id' => $user_id])->get();
        if (isset($getweight['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $getweight,
                'message' => 'Weight listed sucessfully.',

            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }





    public function updateweight(Request $req)
    {
        $data = $req->input();

       

        $updateweight = weight::where('user_id', $data['user_id'])->update($data);

        if ($updateweight) {

            return response()->json([

                'status' => 200,
                'data'   => $data,
                'message' => 'Weight  updated sucessfully.',



            ]);
        } else {

            return response()->json([

                'status' => 400,

                'message' => 'Error in updating.',


            ]);
        }
    }

    function getanalyzer(Request $req)
    {
        $user_id= $req->post('user_id');


        $analyzer= user_analyzer_data::where(['user_id' => $user_id])->get();



       
    



        if (isset($analyzer['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $analyzer,
                'message' => 'analyzer Data listed sucessfully.',

            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }

    function getanalyzername(Request $req)
    {
        $user_id= $req->post('user_id');


        $analyzername= analyzer_data::where(['user_id' => $user_id])->get();
        if (isset($analyzername['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $analyzername,
                'message' => 'analyzer Name listed sucessfully.',

            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }




    public function addanalyzerdata(Request $req){

        try{
            $data=new user_analyzer_data;
           
            $data->name=$req->input('name');
            $data->fat=$req->input('fat');
            $data->clr=$req->input('clr');
            $data->snf=$req->input('snf');

            $data->user_id=$req->input('user_id');

            
        
            
            $data->save();
            return Response()->json([
        
                'status'=>200,
                'data'=>$data,
                'message'=>'Analyzer data Applied Successfully',
        
            ]);
        
           
        
        
        
        }       
        catch(Exception $e){
            return response()->json([
                'status' => 400,
                'message'=>'Error :Analyzer data not applied', 
            ],400);
        }

    }

    public function updateanalyzerdata(Request $req)
    {
        $data = $req->input();

       

        $updatedata = user_analyzer_data::where('user_id', $data['user_id'])->update($data);

        if ($updatedata) {

            return response()->json([

                'status' => 200,
                'data'   => $data,
                'message' => 'analyzer Data  updated sucessfully.',



            ]);
        } else {

            return response()->json([

                'status' => 400,

                'message' => 'Error in updating.',


            ]);
        }
    }


    public function addsetting(Request $req){

        try{
            $setting=new setting;
           
            $setting->weighing=$req->input('weighing');
           
           

            $setting->user_id=$req->input('user_id');

            
        
            
            $setting->save();
            return Response()->json([
        
                'status'=>200,
                'data'=>$setting,
                'message'=>'Analyzer data Applied Successfully',
        
            ]);
        
           
        
        
        
        }       
        catch(Exception $e){
            return response()->json([
                'status' => 400,
                'message'=>'Error :Analyzer data not applied', 
            ],400);
        }

    }


    function getsetting(Request $req)
    {
        $user_id= $req->post('user_id');

        


        $setting= setting::where(['user_id' => $user_id])->get();
        if (isset($setting['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $setting,
                'message' => 'Settings applied sucessfully.',

            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }





    public function updatesetting(Request $req)
    {
        $data = $req->input();

       

        $updatesetting = setting::where('user_id', $data['user_id'])->update($data);

        if ($updatesetting) {

            return response()->json([

                'status' => 200,
                'data'   => $data,
                'message' => 'Weighing Setting  updated sucessfully.',



            ]);
        } else {

            return response()->json([

                'status' => 400,

                'message' => 'Error in updating.',


            ]);
        }
    }












    public function addanalyzersetting(Request $req){

        try{
            $analyzersetting=new analyzer_setting;
           
            $analyzersetting->analyzer=$req->input('analyzer');
           
           

            $analyzersetting->user_id=$req->input('user_id');

            
        
            
            $analyzersetting->save();
            return Response()->json([
        
                'status'=>200,
                'data'=>$analyzersetting,
                'message'=>'Analyzer data Applied Successfully',
        
            ]);
        
           
        
        
        
        }       
        catch(Exception $e){
            return response()->json([
                'status' => 400,
                'message'=>'Error :Analyzer data not applied', 
            ],400);
        }

    }


    function getanalyzersetting(Request $req)
    {
        $user_id= $req->post('user_id');

        


        $setting= analyzer_setting::where(['user_id' => $user_id])->get();
        if (isset($setting['0']->id)) {
            return response()->json([
                'status' => 200,
                'data'   => $setting,
                'message' => 'Settings applied sucessfully.',

            ]);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Oops something went wrong'
            ]);
        }
    }


    public function updateanalyzersetting(Request $req)
    {
        $data = $req->input();

       

        $updatesetting = analyzer_setting::where('user_id', $data['user_id'])->update($data);

        if ($updatesetting) {

            return response()->json([

                'status' => 200,
                'data'   => $data,
                'message' => 'analyzer Setting  updated sucessfully.',



            ]);
        } else {

            return response()->json([

                'status' => 400,

                'message' => 'Error in updating.',


            ]);
        }
    }

}
