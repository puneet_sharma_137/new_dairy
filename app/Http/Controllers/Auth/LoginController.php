<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     
     public function login(Request $request)
    {
       
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);
          $usercount = User::where([['email', $request->email], ['password', $request->password]])->count();
          if($usercount >0)
          {
        $user = User::where([['email', $request->email], ['password', $request->password]])->first();
        Auth::login($user);
        return redirect('/home');
          }
          else
          {
              return redirect()->back()->with('success', 'Email or Password invalid');  
          }
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
