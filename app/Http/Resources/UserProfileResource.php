<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"  => $this->id,
            "image" => url('/') .'/'. $this->image,
            "dsc_code"=> $this->dsc_code,
            "category"=> $this->category,
            "address"=>  $this->address,
            "dsc_name"=> $this->dsc_name,
            "father_name"=> $this->father_name,
            "vehicle"=> $this->vehicle,
            "village"=> $this->village,
            "city"=> $this->city,
            "contact"=> $this->contact,
            "acc_name"=> $this->acc_name,
            "acc_no"=> $this->acc_no,
            "bank_name"=> $this->bank_name,
            "bank_branch"=> $this->bank_branch,
            "ifsc"=> $this->ifsc,
            "pan"=> $this->pan,
            "user_id"=> $this->user_id,
            "created_at"=> $this->created_at,
            "updated_at"=> $this->updated_at,
            "deleted_at"=> $this->deleted_at,
        ];
    }
}