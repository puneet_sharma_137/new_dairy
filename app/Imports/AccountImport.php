<?php
namespace App\Imports;
use App\account;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class AccountImport implements ToModel,WithHeadingRow
{
	/**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new account([
            'vendor_code'     => $row['vendor_code'],
            'vendor_name'     => $row['vendor_name'],
            'father'     => $row['father'],
            'address'     => $row['address'],
            'phone_number'     => $row['phone_number'],
            'mobile_number'     => $row['mobile_number'],
            'city'     => $row['city'],
            'gst_number'     => $row['gst_number'],
            'email_address'     => $row['email_address'],
            'pan_number'     => $row['pan_number'],
            'aadhaar_number'     => $row['aadhaar_number'],
            'bank_name'     => $row['bank_name'],
            'bank_branch'     => $row['bank_branch'],
            'account_number'     => $row['account_number'],
            'ifsc_number'     => $row['ifsc_number'],
            'rate'     => $row['rate'],
            'user_id'     => $row['user_id'],
        ]);
    }
}