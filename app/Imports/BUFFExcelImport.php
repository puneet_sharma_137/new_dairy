<?php

namespace App\Imports;

use App\BuffCsv;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Importable;
class BUFFExcelImport implements ToModel
{
     protected $request;
    
    public function __construct($request){
        $this->user_id = $request->user_id;
        $this->file_type = $request->file_type;
        $this->file_title = $request->file_title;
        $this->file_id = $request->file_id;
        $this->excelName = $request->excelName;
    }
    
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // dd($this->user_id);
        
        return new BuffCsv([
            'user_id'   => $this->user_id,
            // 'user_id'   => 670,
            'file_type' => $this->file_type,
            'file_id' => $this->file_id,
            'file_title' => $this->file_title,
            'file_url' => $this->excelName,
            // 'seq'     => $row[0],
            '0'     => $row[0],
            '7_5'     => $row[1],
            '7_6'     => $row[2],
            '7_7'     => $row[3],
            '7_8'     => $row[4],
            '7_9'     => $row[5],
            '8'     => $row[6],
            '8_1'     => $row[7],
            '8_2'     => $row[8],
            '8_3'     => $row[9],
            '8_4'     => $row[10],
            '8_5'     => $row[11],
            '8_6'     => $row[12],
            '8_7'     => $row[13],
            '8_8'     => $row[14],
            '8_9'     => $row[15],
            '9'     => $row[16],
            '9_1'     => $row[17],
            '9_2'     => $row[18],
            '9_3'     => $row[19],
            '9_4'     => $row[20],
            '9_5'     => $row[21],
            
            
            
        ]);
    }
}