<?php

namespace App\Imports;

use App\CowCsv;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Importable;
class COWExcelImport implements ToModel
{
    protected $userId;
    
    
    public function __construct($id){
        $this->userId = $id;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
        return new CowCsv([
            'user_id'     => $this->userId,
            '1'     => $row[0],
            '2'     => $row[1],
            '3'     => $row[2],
            '4'     => $row[3],
            '5'     => $row[4],
            '6'     => $row[5],
            '7'     => $row[6],
            '8'     => $row[7],
            '9'     => $row[8],
            '10'     => $row[9],
            '11'     => $row[10],
            '12'     => $row[11],
            '13'     => $row[12],
            '14'     => $row[13],
            '15'     => $row[14],
            '16'     => $row[15],
            '17'     => $row[16],
            '18'     => $row[17],
            '19'     => $row[18],
            '20'     => $row[19],
            '21'     => $row[20],
            '22'     => $row[21],
           //

        ]);
    }
}