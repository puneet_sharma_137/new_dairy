<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class account extends Model
{
    //


    protected $fillable = [
        'image' ,

        'vendor_code'   ,

        'vendor_name',

        'father' ,

        'address',

        'phone_number',
        'mobile_number',
        'gst_number' ,
        'email_address' ,
        'pan_number',
        'aadhaar_number',
        'bank_name' ,
        'bank_branch' ,
        'account_number',
        'ifsc_number'
    ];


}
