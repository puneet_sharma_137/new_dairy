<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    //
    protected $fillable = [
        'image',
        'dsc_code',
'category',
'address',
'dsc_name',
'father_name',
'village',
'city',
'contact',
'acc_name',
'acc_no',
'bank_name',
'bank_branch',
'ifsc',
'pan',
'commesion_cow',
'commesion_buffalo',
'head_load',
'extra_fat',
'extra_snf',
'group_profile',
'rate_category',
    ];
}
