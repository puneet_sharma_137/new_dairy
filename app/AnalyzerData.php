<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class AnalyzerData extends Model
{
    protected $table = 'analyzer_data';

}