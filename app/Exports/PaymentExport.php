<?php

namespace App\Exports;
use App\account;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;

class PaymentExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */  
    use Exportable;
    
    public function __construct(int $userid)
    {
        $this->userid = $userid;
    }

    public function collection()
    {
        return DB::table('desktoppayments')->where('user_id', $this->userid)->get();

    }
    
     public function map($data): array {
         return [
             $data->user_id,
             $data->date,
             $data->vendor_code,
             $data->vendor_name,
             $data->bill_number,
             $data->notes,
             $data->amount,
         ];
     }
     
       public function headings(): array
    {
        return [
            'User ID',
            'Date',
            'Vendor Code',
            'Vendor Name',
            'Bill Number',
            'Notes',
            'Amount',
        ];
    }
   
 

}