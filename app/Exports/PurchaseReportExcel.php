<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;

class PurchaseReportExcel implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */  
    use Exportable;
    
    public function __construct(int $userid)
    {
        $this->userid = $userid;
    }

    public function collection()
    {
        return DB::table('dispatches')->where('user_id', $this->userid)->get();

    }
    
     public function map($account): array {
         return [
             $account->user_id,
             $account->date,
             $account->time,
             $account->dairy_code,
             $account->avg_fat,
             $account->avg_snf,
             $account->avg_clr,
             $account->rate,
             $account->weight,
             $account->no_of_cans,
             $account->amount,
             $account->quantity,
         ];
     }
     
       public function headings(): array
    {
        return [
            'User ID',
            'Date',
            'Time',
            'Dairy Code',
            'Avg Fat',
            'Avg SNF',
            'Avg CLR',
            'Rate',
            'Weight',
            'No.of Cans',
            'Amount',
            'Quantity',
        ];
    }
   
 

}