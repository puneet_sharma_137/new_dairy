<?php

namespace App\Exports;
use App\account;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;

class BankAccountExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */  
    use Exportable;
    
    public function __construct(int $userid)
    {
        $this->userid = $userid;
    }

    public function collection()
    {
        return account::where('user_id', $this->userid)->get();

    }
    
     public function map($account): array {
         return [
             $account->user_id,
             $account->vendor_code,
             $account->vendor_name,
             $account->address,
             $account->phone_number,
             $account->mobile_number,
             $account->gst_number,
             $account->pan_number,
             $account->bank_branch,
             $account->account_number,
             $account->ifsc_number,
  
         ];
     }
     
       public function headings(): array
    {
        return [
            'user_id',
            'vendor_code',
            'vendor_name',
            'address',
            'phone_number',
            'mobile_number',
            'gst_number',
            'pan_number',
            'bank_branch',
            'account_number',
            'ifsc_number',
        ];
    }
   
 

}