<?php

namespace App\Exports;
use App\account;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;

class MilkSaleExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */  
    use Exportable;
    
    public function __construct(int $userid)
    {
        $this->userid = $userid;
    }

    public function collection()
    {
        return DB::table('milksales')->where('user_id', $this->userid)->get();

    }
    
     public function map($account): array {
         return [
             $account->user_id,
             $account->date,
             $account->time,
             $account->shift,
             $account->cattle_type,
             $account->vendor_number,
             $account->vendor_name,
             $account->fat,
             $account->weight,
             $account->weighing,
             $account->rate,
             $account->amount,
         ];
     }
     
       public function headings(): array
    {
        return [
            'User ID',
            'Date',
            'Time',
            'Shift',
            'Cattle Type',
            'Vendor Code',
            'Vendor Name',
            'Fat',
            'Weight',
            'Weight Type',
            'Rate',
            'Amount',
        ];
    }
   
 

}