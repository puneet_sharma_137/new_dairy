<?php

namespace App\Exports;
use DB;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;

class MilkCollectionExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */  
    use Exportable;
    
    public function __construct(int $userid)
    {
        $this->userid = $userid;
    }
    
    public function headings(): array
    {
        return [
            'ID',
            'Date',
            'Time',
            'Shift',
            'Cattle Type',
            'Vendor Code',
            'Vendor Name',
            'Fat',
            'SNF',
            'CLR',
            'Weight',
            'Rate',
            'Amount',
            'Weight Type',
            'User ID',
            'Created At',
            'Updated At',
        ];
    }

    public function collection()
    {
        return DB::table('milkcollections')->where('user_id', $this->userid)->get();

    }
    
  
   
 

}