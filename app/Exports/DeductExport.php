<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;

class DeductExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */  
    use Exportable;
    
    public function __construct(int $userid)
    {
        $this->userid = $userid;
    }

    public function collection()
    {
        return DB::table('deducations')->where('user_id', $this->userid)->get();

    }
    
     public function map($account): array {
         return [
             $account->user_id,
             $account->cow_min_fat,
             $account->cow_per_unit,
             $account->fat_cost,
             $account->cow_min_snf,
             $account->snf_per_unit,
             $account->snf_cost,
             $account->buf_min_fat,
             $account->buf_per_unit,
             $account->buff_cost,
             $account->buf_min_snf,
             $account->buf_snf_per_unit,
             $account->buf_nsf_cost,
         ];
     }
     
       public function headings(): array
    {
        return [
            'User ID',
            'cow_min_fat',
            'cow_per_unit',
            'fat_cost',
            'cow_min_snf',
            'snf_per_unit',
            'snf_cost',
            'buf_min_fat',
            'buf_per_unit',
            'buff_cost',
            'buf_min_snf',
            'buf_snf_per_unit',
            'buf_nsf_cost',
        ];
    }
   
 

}