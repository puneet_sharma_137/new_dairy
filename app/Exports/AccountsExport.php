<?php

namespace App\Exports;
use App\account;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;

class AccountsExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */  
    use Exportable;
    
    public function __construct(int $userid)
    {
        $this->userid = $userid;
    }

    public function collection()
    {
        return account::where('user_id', $this->userid)->get();

    }
    
     public function map($account): array {
         return [
             $account->user_id,
              $account->vendor_code,
               $account->vendor_name,
                $account->father,
                 $account->address,
                  $account->phone_number,
                   $account->mobile_number,
                    $account->gst_number,
                     $account->email_address,
                     
            $account->pan_number,
              $account->aadhaar_number,
               $account->bank_name,
                $account->city,
                 $account->bank_branch,
                  $account->account_number,
                   $account->ifsc_number,
              
                     $account->rate,
         ];
     }
     
       public function headings(): array
    {
        return [
            'user_id',
            'vendor_code',
            'vendor_name',
            'father',
            'address',
            'phone_number',
            'mobile_number',
            'gst_number',
            'email_address',
            'pan_number',
            'aadhaar_number',
            'bank_name',
            'city',
            'bank_branch',
            'account_number',
            'ifsc_number',
            'rate',
           
            
              
        ];
    }
   
 

}