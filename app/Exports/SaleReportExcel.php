<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;

class SaleReportExcel implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */  
    use Exportable;
    
    public function __construct(int $userid)
    {
        $this->userid = $userid;
    }

    public function collection()
    {
        return DB::table('milksales')->where('user_id', $this->userid)->get();

    }
    
     public function map($account): array {
         return [
             
                $account->user_id,
                $account->date,
                $account->time,
                $account->shift,
                $account->cattle_type,
                $account->vendor_number,	
                $account->vendor_name,
                $account->fat,
                $account->weight,
                $account->rate,
                $account->amount,
             
         ];
     }
     
       public function headings(): array
    {
        return [
            'User ID',
            'Date',
            'Time',
            'Shift',
            'cattle_type',
            'vendor_number',
            'vendor_name',
            'fat',
            'Weight',
            'rate',
            'amount',
          
        ];
    }
   
 

}