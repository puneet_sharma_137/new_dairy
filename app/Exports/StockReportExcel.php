<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;

class StockReportExcel implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */  
    use Exportable;
    
    public function __construct(int $userid)
    {
        $this->userid = $userid;
    }

    public function collection()
    {
        return DB::table('stockupdates')->where('user_id', $this->userid)->get();

    }
    
     public function map($account): array {
         return [
             
                $account->date,
                $account->time,
                $account->vendor_number,
                $account->vendor_name,
                $account->type,
                $account->quantity,
                $account->amount,
             
               
             
         ];
     }
     
       public function headings(): array
    {
        return [
          ' date',
    'time',
    'vendor_number',
    'vendor_name',
    'Item',
    'quantity',
    'amount',
          
        ];
    }
   
 

}