<?php

namespace App\Exports;
use App\account;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AccountsExport implements FromQuery
{
    /**
    * @return \Illuminate\Support\Collection
    */  
    // use Exportable;

    public function query()
    {
       $data=account::all()->where('user_id', '=', $this->id);
        return $data;

    }
    public function map($bulk): array
    {
        return $bulk;
    }

}