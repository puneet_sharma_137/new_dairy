<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;

class BonusExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */  
    use Exportable;
    
    public function __construct(int $userid)
    {
        $this->userid = $userid;
    }

    public function collection()
    {
        return DB::table('bonus')->where('user_id', $this->userid)->get();

    }
    
     public function map($account): array {
         return [
             $account->user_id,
             $account->cow,
             $account->cow_per_unit,
             $account->cow_cost,
             $account->buff,
             $account->buff_per_unit,
             $account->buff_cost,
         ];
     }
     
       public function headings(): array
    {
        return [
            'User ID',
            'cow_min_fat',
            'cow_per_unit',
            'cow_cost',
            'buf_min_fat',
            'buf_per_unit',
            'buf_cost'
        ];
    }
   
 

}