<?php

namespace App\Exports;
use App\account;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;

class StockUpdateExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */  
    use Exportable;
    
    public function __construct(int $userid)
    {
        $this->userid = $userid;
    }

    public function collection()
    {
        return DB::table('stockupdates')->where('user_id', $this->userid)->get();

    }
    
     public function map($data): array {
         return [
             $data->user_id,
             $data->vendor_number,
             $data->vendor_name,
             $data->quantity,
             $data->type,
             $data->amount,
         ];
     }
     
       public function headings(): array
    {
        return [
            'User ID',
            'Vendor Code',
            'Vendor Name',
            'Quantity',
            'Type',
            'Amount',
        ];
    }
   
 

}