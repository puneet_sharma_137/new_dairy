<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;

class ItemSalesExport implements FromCollection, WithMapping, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */  
    use Exportable;
    
    public function __construct(int $userid)
    {
        $this->userid = $userid;
    }

    public function collection()
    {
        return DB::table('itemsales')->where('user_id', $this->userid)->get();

    }
    
     public function map($account): array {
         return [
             $account->user_id,
             $account->date,
             $account->time,
             $account->code,
             $account->name,
             $account->item,
             $account->units,
            $account->rate,
            $account->quantity,
            $account->amount,
         ];
     }
     
       public function headings(): array
    {
        return [
            'User ID',
            'Date',
            'Time',
            'Code',
            'Name',
            'Item',
            'Units',
            'Rate',
            'Quantity',
            'Amount',
        ];
    }
   
 

}